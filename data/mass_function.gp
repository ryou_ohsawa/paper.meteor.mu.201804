#!/usr/bin/gnuplot
reset
set terminal pdfcairo size 16cm,10cm font 'Ubuntu,16'
set output '../paper/figs/mass_function.pdf'
set encoding iso

mu_j06='<zcat mass_function_j06.sav.gz'
mu_h05='<zcat mass_function_h05.sav.gz'

set bar 0
set lmargin 8
set bmargin 2.3
set tmargin 0
set rmargin 2

set log x
set log y

set size 1.0,0.7
set origin 0,0.3

set size 1.0,0.96
set origin 0,0.04
set xr [7e-7:2e+0]
set yr [3e-7:2e+1]
set xtics 10 format '10^{%-2L}'
set ytics 1e-8,10,1e+4 format ' 10^{%-2L}'
set xlabel 'Meteoroid Mass (g)' font ',20'
set ylabel 'Cumulative Number Flux (h^{-1}&{,}km^{-2})' font ',20' offset -0.5,0
set tics front

# N_0 = 2.5e-4; s = 2.0
# func(x) = 10**(log10(N_0)-(s-1)*log10(x))
# fit [5e-3:1e+1] func(x) mu_j06 u 1:6:($6/10) yerror via N_0,s
# func_j06(x) = 10**(log10(N_0_j06)-(s_j06-1)*log10(x))
# N_0_j06 = N_0; s_j06 = s

N_0 = 1.0e-6; s = 2.45
func(x) = 10**(log10(N_0)-(s-1)*log10(x))
fit [5e-4:5e-1] func(x) mu_h05 u 1:6:($6/10) yerror via N_0,s
func_h05(x) = 10**(log10(N_0_h05)-(s_h05-1)*log10(x))
N_0_h05 = N_0; s_h05 = s

#set grid x y
set mxtics 10
set mytics 10

#set label 1 '(c)' left at graph 0, graph 1 \
#font 'Ubuntu-Bold,22' offset 1,-1

eps=1e-9
clip(x,lx,hx) = ((x<hx)?(x>lx)?x:1/0:1/0)
plot \
  mu_h05 u 1:($6+3*$7+eps):(clip($6-3*$7,eps,$6+eps)) \
  w filledc lc 7 fs solid 0.1 not, \
  mu_h05 u 1:($6+$7+eps):($6-$7-eps) w filledc lc 7 fs solid 0.3 not, \
  mu_h05 u 1:6 w l lw 2 lc 7 \
  t 'Mass Function (Hill+ 2005)', \
  func_h05(clip(x,2e-5,0.8)) lc 6 lw 2 dt (16,8) \
  t sprintf('Power-law regression (s = %.2f)', s_h05)

# mu_j06 u 1:($6+3*$7+eps):(clip($6-3*$7,eps,$6+eps)) \
# w filledc lc 7 fs solid 0.1 not, \
# mu_j06 u 1:($6+$7+eps):($6-$7-eps) w filledc lc 7 fs solid 0.3 not, \
# mu_j06 u 1:6 w l lw 2 lc 7 \
# t 'Mass Function (Jenniskens 2006)', \
# func_j06(clip(x,1e-4,20)) lc 6 lw 2 dt (16,8) \
# t sprintf('Power-law regression (s = %.2f)', s_j06),\
