function param=fit_with_error(X,sigma=3.0,niter=5,nloop=100)
  pkg load statistics
  N = length(X);
  x = X(:,1); y = X(:,2); err = X(:,3:4);
  for n=1:nloop
    z = randsample(N,N,true);
    xe = (x+normrnd(0,err(1),size(x,1),size(x,2)))(z);
    ye = (y+normrnd(0,err(2),size(y,1),size(y,2)))(z);
    param(n,:) = inside(xe,ye,sigma,niter);
  endfor
  param = [mean(param,1); std(param,[],1)];
endfunction

function param=inside(x,y,sigma,niter)
  idx = true(size(x));
  l = linspace(min(x),max(x),30)';
  m = linspace(min(y),max(y),30)';
  for n=1:niter
    p = polyfit(x(idx),y(idx),1);
    q = polyfit(y(idx),x(idx),1);
    sigma_p = std(polyval(p,x)-y);
    sigma_q = std(polyval(q,y)-x);
    idx = abs(polyval(p,x)-y)<sigma_p*sigma ...
          & abs(polyval(q,y)-x)<sigma_q*sigma;
  endfor
  t = [atan2(p(1),1); atan2(1/q(1),1)];
  r(1,1) = tan(mean(t));
  r(1,2) = median(y-x*r(1));
  q = [1/q(1),-q(2)/q(1)];
  param = [r range([p;q])/2.0];
endfunction
