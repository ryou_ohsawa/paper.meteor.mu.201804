#!/bin/bash
TARGET=${1?}

echo 'IAUnum,code,name,ra,dec,Vg,a,q,e,peri,node,incl'
grep '|'  ${TARGET} \
  | awk -F'|' '{print $2,$4,$5,$9,$10,$13,$14,$15,$16,$17,$18,$19}' \
  | sed 's/\s\s*"/"/g;s/"/|/g;s/||/|/g;/||/d' \
  | sed 's/^|//;s/|$//;s/|/,/g' | tac \
  | sort -k 1.7,1.12 -u | sort -k 1.1,1.7 \
  | sed 's/^|//;s/|$//;s/|/,/g'
