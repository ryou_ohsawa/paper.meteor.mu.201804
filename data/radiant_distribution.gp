#!/usr/bin/gnuplot
reset
set terminal pdfcairo size 16cm,8.8cm font 'Ubuntu,16'
set output '../paper/figs/radiant_distribution.pdf'
set encoding iso
set datafile separator ','

iccd='<gunzip -c mu+iccd_main.csv.gz'
tomoe='<gunzip -c mu+tomoe_main.csv.gz'

wrapd(x) = (x>=360?wrapd(x-360):x<0?wrapd(x+360):x)
cosd(x) = cos(pi*x/180.)
sind(x) = sin(pi*x/180.)
hammer_n(l,b) = sqrt(1+cosd(b)*cosd(l/2.))
hammer_x(l,b) = 2*sqrt(2)*cosd(b)*sind(l/2.)/hammer_n(l,b)
hammer_y(l,b) = sqrt(2)*sind(b)/hammer_n(l,b)

set tmargin 0
set bmargin -1
set lmargin 0
set rmargin 0

vth = 45.0
set size ratio -1
set parametric
set tr [0:1]
set xtics auto
set ytics auto
set xr [-2.60:2.60]
set yr [-1.25:1.35]
unset xlabel
unset ylabel
unset xtics
unset ytics
unset border
set key bmargin left Left horizontal inv rev samplen 1 font ',12'

labelcolor='gray40'
set label 1 'Sun-Centered Ecliptic Longitude (deg)' font ',12' \
front center at hammer_x(0,-40),hammer_y(0,-40) \
offset 0,-1 tc rgb labelcolor
set label 2 'Ecliptic Latitude (deg)' font ',12' \
front center at hammer_x(-150,0),hammer_y(-150,0) \
rotate by 90 offset -1,0 tc rgb labelcolor

do for [i=-5:5] {
set label (i+10) sprintf('%d{\272}',wrapd(270-30*i)) \
front center at hammer_x(30*i,-60),hammer_y(30*i,-60) \
offset 0,0.5 font ',10' tc rgb labelcolor
}
do for [i=-2:3] {
set label (i+30) sprintf('%d{\272}',20*i) \
front center at hammer_x(-150,20*i),hammer_y(-150,20*i) \
offset 1,0.5 font ',10' tc rgb labelcolor
}

plot \
  for [l=-150:150:30] \
    hammer_x(l,180*t-90),hammer_y(l,180*t-90) \
    lw 0.5 dt (4,4) lc rgb 'gray30' not, \
  for [b=-80:80:20] \
    hammer_x(360*t-180,b),hammer_y(360*t-180,b) \
    lw 0.5 dt (4,4) lc rgb 'gray30' not, \
  hammer_x(-180,180*t-90),hammer_y(-180,180*t-90) \
  lw 0.5 lc rgb 'gray30' not, \
  hammer_x(180,180*t-90),hammer_y(180,180*t-90) \
  lw 0.5 lc rgb 'gray30' not, \
  iccd u 54:($20<vth?$55:1/0) w p lc 4 pt 6 ps 0.6 \
  t sprintf('MU + CCD (v<%.0fkm)',vth), \
  iccd u 54:($20>vth?$55:1/0) w p lc 2 lw 1.5 pt 6 ps 0.6 \
  t sprintf('MU + CCD (v>%.0fkm)',vth), \
  tomoe u 126:($92<vth?$127:1/0) w p lc 4 pt 4 ps 0.6 \
  t sprintf('MU + Tomo-e (v<%.0fkm)',vth), \
  tomoe u 126:($92>vth?$127:1/0) w p lc 2 lw 1.5 pt 4 ps 0.6 \
  t sprintf('MU + Tomo-e (v>%.0fkm)',vth)
