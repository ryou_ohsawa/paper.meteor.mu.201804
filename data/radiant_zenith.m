#!/usr/bin/octave

function z = radiant_zenith(data_array,L=1e6)
  pkg load nan
  cosd = @(x) cos(pi*x/180.);
  sind = @(x) sin(pi*x/180.);
  hx = @(h,a,z) h.*cosd(a).*sind(z);
  hy = @(h,a,z) h.*sind(a).*sind(z);
  hz = @(h,a,z) h.*cosd(z);
  vx = @(h1,a1,z1,h2,a2,z2,t) ...
     hx(h1,a1,z1)+t.*(hx(h2,a2,z2)-hx(h1,a1,z1));
  vy = @(h1,a1,z1,h2,a2,z2,t) ...
    hy(h1,a1,z1)+t.*(hy(h2,a2,z2)-hy(h1,a1,z1));
  t = @(h1,a1,z1,h2,a2,z2) ...
   (L-hz(h1,a1,z1))./(hz(h2,a2,z2)-hz(h1,a1,z1));
  tx = @(h1,a1,z1,h2,a2,z2) ...
    vx(h1,a1,z1,h2,a2,z2,t(h1,a1,z1,h2,a2,z2));
  ty = @(h1,a1,z1,h2,a2,z2) ...
    vy(h1,a1,z1,h2,a2,z2,t(h1,a1,z1,h2,a2,z2));
  tr = @(h1,a1,z1,h2,a2,z2) ...
    sqrt(tx(h1,a1,z1,h2,a2,z2).^2+ty(h1,a1,z1,h2,a2,z2).^2);
  v1 = data_array(:,[22,24,25]);
  v2 = data_array(:,[23,26,27]);
  r = tr(v1(:,1),v1(:,2),v1(:,3),v2(:,1),v2(:,2),v2(:,3));
  z = atan2(L,r)/pi*180.;
endfunction
