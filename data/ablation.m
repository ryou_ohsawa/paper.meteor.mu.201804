function dy = ablation(y,m)
  ## y = [h,v,T,t]
  h = y(1); v = y(2); T = y(3);
  ## parameters
  G = 1.0;              # drag coefficient [none]
  A = 1.21;             # shape factor [none]
  l = 1.0;              # heat transfer coefficient [none]
  s = 5.67e-8;          # Stefan-Boltzmann constant [W/m2/K4]
  em = 0.9;             # emissivity [none]
  Ta = 280;             # effective atmospheric temperature [K]
  c = 1200;             # specific heat of meteoroid [1200 J/K/kg]
  L = 6.0e6;            # latent heat (fusuion + evapolation) [J/kg]
  kB = 1.381e-23;       # Boltzmann constant [J/K]
  rhom = 3300;          # meteoroid mass density [kg/m3]
  rhoa  = calc_rhoa(h); # atmospheric mass density [kg/m3]
  mu = 50;              # mean molecular mass of ablated material
  zin = pi/4.0;         # incident angle [rad]
  mrho  = m^(1/3)*rhom^(2/3);
  dTatm = 0.5*l*rhoa*v^3;
  dTrad = 4*s*em*(T^4-Ta^4);
  dTlat = -L*Pv(T)*sqrt(mu/2/pi/kB/T);
  dhdt = -v*cos(zin);
  dvdt = -G*A/mrho*rhoa*v^2;
  dTdt =  (A/c/mrho)*(dTatm - dTrad + dTlat);
  dmdt = -4*A*(m/rhom)^(2/3)*Pv(T)*sqrt(mu/2/pi/kB/T);
  dy = [dhdt; dvdt; dTdt; 1.0]/dmdt;
endfunction

function pv = Pv(T)
  ## saturated vapor pressure of meteoroid
  C1 = 10.6; C2 = 13500;
  pv = 10**(C1 - C2./T);
endfunction

function r = calc_rhoa(h)
  ## atmospheric mass density [kg/m3]
  a = load('./msise-90_massdensity.dat');
  r = interp1(a(:,1),a(:,2),h/1e3);
endfunction
