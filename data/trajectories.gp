#!/usr/bin/gnuplot
reset
set terminal pdfcairo size 16cm,17.5cm font 'Ubuntu,12'
set output '../paper/figs/meteor_trajectories_all.pdf'
set encoding iso
set datafile separator ','


tomoe='<gunzip -c all_mu+tomoe_candidates.csv.gz'
mu='<gunzip -c all_mu+tomoe_candidates.csv.gz | sort -t, -k 75,75 -u'
sensor='<gunzip -c sensor_frame.csv.gz'

set tmargin 2.0
set bmargin 2.0
set rmargin 0.0
set lmargin 2.0

set object 1 circle front at 233.57, 28.88 size 0.1 \
lw 1.5 fs solid 0.2 fc rgb 'dark-violet'
set object 2 circle front at 233.57, 31.28 size 0.1 \
lw 1.5 fs solid 0.2 fc rgb 'dark-green'
set object 3 circle front at 233.57, 33.55 size 0.1 \
lw 1.5 fs solid 0.2 fc rgb 'navy'

set multiplot
set size 1,1
set origin 0,0
set xlabel 'Azimuth at Kiso Observatory (deg)' font ',20'
set ylabel 'Elevation at Kiso Observatory (deg)' font ',20'
unset border
set xr [0:10]; unset xtics
set yr [0:10]; unset ytics
set key samplen 4 tmargin horizontal font ',14' width -2
plot \
  -1 lw 2 lc 2 t 'MU radar Streak', \
  -1 lw 2 lc 7 t 'Tomo-e Gozen Streak'
unset xlabel
unset ylabel
set key samplen 1 tmargin horizontal font ',14' spacing 3 width 0
plot \
  -1 w circle lw 1.5 fs solid 0.2 fc rgb 'dark-violet' \
  t '100&{,}km', \
  -1 w circle lw 1.5 fs solid 0.2 fc rgb 'dark-green' \
  t '110&{,}km', \
  -1 w circle lw 1.5 fs solid 0.2 fc rgb 'navy' \
  t '120&{,}km'

unset key
set tmargin 0
set lmargin 0
set bmargin 0
set rmargin 0
unset xlabel
unset ylabel
day(x,d,D) = (d==D)?x:1/0
set border 31
set size ratio -1
set xr [229:241]
set yr [23.2:35.5]
set ytics 0,2,40
set mytics 2
set xtics 200,2,250
set mxtics 2
set grid x y

origin_x(n) = (n%2==0)?0.08:0.57
origin_y(n) = (n/2==0)?0.52:0.06

do for [day=18:21] {
set size 0.42
set origin origin_x(day-18),origin_y(day-18)
set label 1 sprintf('2018-04-%02d',day) left at graph 0,1 \
font 'Ubuntu-Bold,14' offset 0.2,0.8
plot \
  sensor u (day($6,$3,day)):7:($8-$6):($9-$7) w vec nohead \
  lw 0.8 lc rgb 'gray60' not, \
  sensor u (day($8,$3,day)):9:($10-$8):($11-$9) w vec nohead \
  lw 0.8 lc rgb 'gray60' not, \
  sensor u (day($10,$3,day)):11:($12-$10):($13-$11) w vec nohead \
  lw 0.8 lc rgb 'gray60' not, \
  sensor u (day($12,$3,day)):13:($6-$12):($7-$13) w vec nohead \
  lw 0.8 lc rgb 'gray60' not, \
  mu u (day($29,$38,day)):30 w p pt 7 ps 0.3 lc 2 not, \
  mu u (day($32,$38,day)):33 w p pt 6 ps 0.5 lc 2 not, \
  mu u (day($52,$38,day)):53:($54-$52):($55-$53) \
  w vec nohead dt (4,8) lw 0.2 lc rgb 'gray60' not, \
  mu u (day($29,$38,day)):30:($32-$29):($33-$30) \
  w vec nohead lw 0.5 lc 2 t 'MU radar Streak', \
  tomoe u (day($56,$38,day)):57:(-$59):(-$60) \
  w vec nohead lw 2 lc 6 not, \
  tomoe u (day($46,$38,day)):47:($48-$46):($49-$47) \
  w vec nohead lw 1 lc 7 t 'Tomo-e Streak'
}
unset multiplot

set terminal pdfcairo size 14cm,14.5cm font 'Ubuntu,12'
set output '../paper/figs/meteor_trajectories.pdf'
do for [day=18:21] {

set tmargin 2.0
set bmargin 2.0
set rmargin 1.0
set lmargin 2.0

unset label
set multiplot
set size 1,1
set origin 0,0
set xlabel 'Azimuth at Kiso Observatory (deg)' font ',20'
set ylabel 'Elevation at Kiso Observatory (deg)' font ',20'
unset border
set xr [0:10]; unset xtics
set yr [0:10]; unset ytics
set key samplen 4 tmargin horizontal font ',14' spacing 1 width -2
plot \
  -1 lw 2 lc 2 t 'MU radar Streak', \
  -1 lw 2 lc 7 t 'Tomo-e Gozen Streak'
unset xlabel
unset ylabel
set key samplen 1 tmargin horizontal font ',14' spacing 3 width 0
plot \
  -1 w circle lw 1.5 fs solid 0.2 fc rgb 'dark-violet' \
  t '100&{,}km', \
  -1 w circle lw 1.5 fs solid 0.2 fc rgb 'dark-green' \
  t '110&{,}km', \
  -1 w circle lw 1.5 fs solid 0.2 fc rgb 'navy' \
  t '120&{,}km'

unset key
set tmargin 0
set lmargin 0
set bmargin 0
set rmargin 0
unset xlabel
unset ylabel
day(x,d,D) = (d==D)?x:1/0
set border 31
set size ratio -1
set xr [229:241]
set yr [23.2:35.5]
set ytics 0,2,40
set mytics 2
set xtics 200,2,250
set mxtics 2
set grid x y

origin_x(n) = (n%2==0)?0.11:0.57
origin_y(n) = (n/2==0)?0.53:0.08

set size 0.85
set origin origin_x(2),origin_y(2)
set label 1 sprintf('2018-04-%02d',day) left at graph 0,1 \
font 'Ubuntu-Bold,20' offset 0.2,0.8
plot \
  sensor u (day($6,$3,day)):7:($8-$6):($9-$7) w vec nohead \
  lw 0.8 lc rgb 'gray60' not, \
  sensor u (day($8,$3,day)):9:($10-$8):($11-$9) w vec nohead \
  lw 0.8 lc rgb 'gray60' not, \
  sensor u (day($10,$3,day)):11:($12-$10):($13-$11) w vec nohead \
  lw 0.8 lc rgb 'gray60' not, \
  sensor u (day($12,$3,day)):13:($6-$12):($7-$13) w vec nohead \
  lw 0.8 lc rgb 'gray60' not, \
  mu u (day($29,$38,day)):30 w p pt 7 ps 0.3 lc 2 not, \
  mu u (day($32,$38,day)):33 w p pt 6 ps 0.5 lc 2 not, \
  mu u (day($52,$38,day)):53:($54-$52):($55-$53) \
  w vec nohead dt (4,8) lw 0.2 lc rgb 'gray60' not, \
  mu u (day($29,$38,day)):30:($32-$29):($33-$30) \
  w vec nohead lw 0.5 lc 2 t 'MU radar Streak', \
  tomoe u (day($56,$38,day)):57:(-$59):(-$60) \
  w vec nohead lw 2 lc 6 not, \
  tomoe u (day($46,$38,day)):47:($48-$46):($49-$47) \
  w vec nohead lw 1 lc 7 t 'Tomo-e Streak'
unset multiplot
}
