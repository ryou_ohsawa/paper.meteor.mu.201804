#!/usr/bin/gnuplot
reset
set terminal pdfcairo size 16cm,11cm font 'Ubuntu,16'
set output '../paper/figs/magnitude_distribution.pdf'
set encoding iso
set datafile separator ','


iccd='<gunzip -c mu+iccd_main.csv.gz'
tomoe='<gunzip -c mu+tomoe_main.csv.gz'

hist(x,w) = w*(floor(x/w)+0.5)
bw=0.5
set boxwidth bw

set print 'stats_vmag_iccd.txt'
stats iccd u 75 name 'iccd'
set print 'stats_vmag_tomoe.txt'
stats tomoe u 141 name 'tomoe'

set lmargin 7
set bmargin 0
set tmargin 0
set rmargin 2
set xr [0.0:12.5]
set xtics -1,1,13 format ''

set multiplot
set size 1,0.42
set origin 0,0.56
set yr [0:18]
set ytics 0,5,100
plot iccd u (hist($75,bw)):(1.0) s freq w boxes \
     lw 1.5 lc 1 fs pattern 6 \
     t sprintf('MU + CCD (%d events)',iccd_records)

set origin 0,0.12
set yr [0:58]
set ytics 0,10,100
set xtics -1,1,13 format '%.0f'
set xlabel 'Absolute magnitude in the V-band' \
font ',20' offset 0,0.2
set label 1 'Frequency' font ',20' \
center at screen 0.0,0.5 rotate by 90 offset 1.5,0
plot tomoe u (hist($141,bw)):(1.0) s freq w boxes \
     lw 1.5 lc 7 fs pattern 7 \
     t sprintf('MU + Tomo-e (%d events)',tomoe_records)
unset multiplot
