#!/usr/bin/gnuplot
reset
set terminal pdfcairo size 16cm,16cm font 'Ubuntu,16'
set output '../paper/figs/luminosity_function.pdf'
set encoding iso

mu='luminosity_function.sav'

set bar 0
set lmargin 8
set bmargin 2
set tmargin 0
set rmargin 2

unset log x
set log y

set size 1.0,0.7
set origin 0,0.3

set xr [-2.8:13.2]
set xtics -6,2,14 format ''
set ytics auto format ' 10^{%L}'
unset xlabel

set size 1.0,0.96
set origin 0,0.04
set auto y
set yr [1e-7:5e+1]
set xtics format '%.0f'
set ytics 1e-8,10,1e+4 format ' 10^{%-2L}'
set xlabel 'Absolute Magnitude in the V-band'
set ylabel 'Cumulative Number Flux (h^{-1}&{,}km^{-2})'
set xlabel font ',20'
set ylabel font ',20' offset -0.5,0
set tics front


N_0 = 1e-2; r = 3.1
func(x) = N_0*10**(log10(r)*(x-3))
fit [-1.5:9.5] func(x) mu u 2:6:($6/1e1) yerror via N_0,r

#set grid x y
set mxtics 2
set mytics 10

set label 1 '(c)' left at graph 0, graph 1 \
font 'Ubuntu-Bold,22' offset 1,-1

clip(x,lx,hx) = ((x<hx)?(x>lx)?x:1/0:1/0)
plot \
  mu u 2:($6+3*$7):($6-3*$7) w filledc lc 7 fs solid 0.1 not, \
  mu u 2:($6+$7):($6-$7) w filledc lc 7 fs solid 0.3 not, \
  mu u 2:6 w l lw 2 lc 7 \
  t 'Luminosity Function of Sporadic Meteors', \
  func(clip(x,-1.5,9.5)) lc 6 lw 3 dt (16,8) \
  t sprintf('Power-law regression curve (r = %.2f)', r)


set terminal pdfcairo size 16cm,10cm font 'Ubuntu,16'
set output '../paper/figs/luminosity_function_monthly.pdf'
set label 1 '(a-1)' left at graph 0, graph 1 \
font 'Ubuntu-Bold,22' offset 1,-1.2

set bmargin 3
mu='luminosity_function_monthly.sav'

do for [n=0:11] {
stats [4.8:5.0][0:10] mu i n u 2:6 name sprintf('month%02d', n+1) nooutput
}

set key Right bottom
set yr [2e-4:8e2]
set ylabel 'Normalized Cumulative Number Flux'
plot \
  mu i  0 u 2:($6/month01_mean_y) w l lw 1 lc 1 \
  dt (24,0) t 'January (3.37)', \
  mu i  1 u 2:($6/month02_mean_y) w l lw 1 lc 7 \
  dt (24,0) t 'February (3.35)', \
  mu i  2 u 2:($6/month03_mean_y) w l lw 1 lc 6 \
  dt (24,0) t 'March (3.43)', \
  mu i  3 u 2:($6/month04_mean_y) w l lw 1 lc 4 \
  dt (24,0) t 'April (3.45)', \
  mu i  4 u 2:($6/month05_mean_y) w l lw 1 lc 1 \
  dt (24,8) t 'May (3.41)', \
  mu i  5 u 2:($6/month06_mean_y) w l lw 1 lc 7 \
  dt (24,8) t 'June (3.51)', \
  mu i  6 u 2:($6/month07_mean_y) w l lw 1 lc 6 \
  dt (24,8) t 'July (3.60)', \
  mu i  7 u 2:($6/month08_mean_y) w l lw 1 lc 4 \
  dt (24,8) t 'August (3.38)', \
  mu i  8 u 2:($6/month09_mean_y) w l lw 1 lc 1 \
  dt (24,8,8,8) t 'September (3.59)', \
  mu i  9 u 2:($6/month10_mean_y) w l lw 1 lc 7 \
  dt (24,8,8,8) t 'October (3.60)', \
  mu i 10 u 2:($6/month11_mean_y) w l lw 1 lc 6 \
  dt (24,8,8,8) t 'November (3.71)', \
  mu i 11 u 2:($6/month12_mean_y) w l lw 1 lc 4 \
  dt (24,8,8,8) t 'December (3.45)'


set output '../paper/figs/luminosity_function_hourly.pdf'
set label 1 '(b-1)' left at graph 0, graph 1

mu='luminosity_function_hourly.sav'

do for [n=0:11] {
stats [4.9:5.1][0:10] mu i n u 2:6 name sprintf('hour%02d', n+1) nooutput
}

set key bottom
set yr [4e-4:8e2]
set ylabel 'Normalized Cumulative Number Flux'
plot \
  mu i  0 u 2:($6/hour01_mean_y) w l lw 1 lc 1 \
  dt (24,0) t '00:00{/Symbol \276}02:00 (3.45)', \
  mu i  1 u 2:($6/hour02_mean_y) w l lw 1 lc 7 \
  dt (24,0) t '02:00{/Symbol \276}04:00 (3.27)', \
  mu i  2 u 2:($6/hour03_mean_y) w l lw 1 lc 6 \
  dt (24,0) t '04:00{/Symbol \276}06:00 (3.23)', \
  mu i  3 u 2:($6/hour04_mean_y) w l lw 1 lc 4 \
  dt (24,0) t '06:00{/Symbol \276}08:00 (3.27)', \
  mu i  4 u 2:($6/hour05_mean_y) w l lw 1 lc 1 \
  dt (24,8) t '08:00{/Symbol \276}10:00 (3.04)', \
  mu i  5 u 2:($6/hour06_mean_y) w l lw 1 lc 7 \
  dt (24,8) t '10:00{/Symbol \276}12:00 (3.12)', \
  mu i  6 u 2:($6/hour07_mean_y) w l lw 1 lc 6 \
  dt (24,8) t '12:00{/Symbol \276}14:00 (3.16)', \
  mu i  7 u 2:($6/hour08_mean_y) w l lw 1 lc 4 \
  dt (24,8) t '14:00{/Symbol \276}16:00 (3.18)', \
  mu i  8 u 2:($6/hour09_mean_y) w l lw 1 lc 1 \
  dt (24,8,8,8) t '16:00{/Symbol \276}18:00 (3.45)', \
  mu i  9 u 2:($6/hour10_mean_y) w l lw 1 lc 7 \
  dt (24,8,8,8) t '18:00{/Symbol \276}20:00 (3.60)', \
  mu i 10 u 2:($6/hour11_mean_y) w l lw 1 lc 6 \
  dt (24,8,8,8) t '20:00{/Symbol \276}22:00 (3.65)', \
  mu i 11 u 2:($6/hour12_mean_y) w l lw 1 lc 4 \
  dt (24,8,8,8) t '22:00{/Symbol \276}24:00 (3.63)'


set terminal pdfcairo size 16cm,5cm font 'Ubuntu,16'
set output '../paper/figs/population_index_monthly.pdf'
set label 1 '(a-2)' left at graph 0, graph 1

set tmargin 0.5
set bmargin 2.5

t='population_index_monthly.sav'
target=sprintf('<gunzip -c %s %s %s %s | sed "/#/d;/^\s*$/d"',t,t,t,t)
target=sprintf('<cat %s %s %s %s | sed "/#/d;/^\s*$/d"',t,t,t,t)

unset log x
set xr [-2.5:27.5]
set xtics ('1' 1)
do for [m=-3:30:3] {
set xtics add (sprintf("%d",((m+12)%12)+1) m+1)
set xtics add ("" m+2 1, "" m+3 1)
}
set xlabel 'Month' offset 0,0.2
unset log y
set yr [3.28:3.75]
set ytics 0.1 format '%4.1f'
set mytics 5
set ylabel 'r-index'

plot \
  target u ($0+1-12):1 w lp lc 1 pt 6 not
#  3.43+0.16*sin(2*pi*(x/12.0-0.3)) w l lc 7 dt (8,8) not, \


set output '../paper/figs/population_index_hourly.pdf'
set label 1 '(b-2)' left at graph 0, graph 1

t='population_index_hourly.sav'
target=sprintf('<cat %s %s %s %s | sed "/#/d;/^\s*$/d"',t,t,t,t)

unset log x
set xr [-2.0:50.0]
set xtics ('00' 0)
do for [m=-4:60:4] {
set xtics add (sprintf("%02d",((m+24)%24)) m)
set xtics add ("" m+1 1, "" m+2 1, "" m+3 1)
}
set xlabel 'Local Time (hour)' offset 0,0.2
unset log y
set yr [2.95:3.75]
set ytics 0.1 format '%4.1f'
set mytics 5
set ylabel 'r-index'

plot \
  target u (2*$0+1-24):1 w lp lc 1 pt 6 not
