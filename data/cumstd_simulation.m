#!/usr/bin/octave

function unc = cumstd_simulation(array,weight,N=10,Nmax=1000)
  warning('off','all');
  x0 = array(1);
  unc = zeros(size(array));
  Nmax = min(Nmax,numel(array));
  for n=2:Nmax
    fflush(stdout);
    x = array(n);
    nx = poissrnd(n,N,1);
    sample = [];
    for nn=1:N
      t=unifrnd(x,x0,nx(nn),1);
      sample(nn,1) = sum(interp1(array,weight,t));
    endfor
    unc(n,1) = std(sample);
    printf('sample %d done\n', n);
  endfor
  unc(1,1) = interp1(array(2:10),unc(2:10),x0,'pchip','EXTRAP');
endfunction
