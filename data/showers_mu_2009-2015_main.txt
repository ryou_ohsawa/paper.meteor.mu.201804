## alpha Capricornids (CAP)            : 72
## Southern Taurids (STA)              : 448
## Geminids (GEM)                      : 1034
## Southern delta Aquariids (SDA)      : 163
## Perseids (PER)                      : 405
## Orionids (ORI)                      : 2049
## October Draconids (DRA)             : 85
## Quadrantids (QUA)                   : 246
## eta Virginids (EVI)                 : 90
## kappa Cygnids (KCG)                 : 14
## Leonids (LEO)                       : 154
## Ursids (URS)                        : 6
## sigma Hydrids (HYD)                 : 82
## Northern Taurids (NTA)              : 231
## Andromedids (AND)                   : 22
## December Monocerotids (MON)         : 142
## Comae Berenicids (COM)              : 39
## alpha Virginids (AVB)               : 22
## Leonis Minorids (LMI)               : 85
## epsilon Geminids (EGE)              : 84
## Northern delta Aquariids (NDA)      : 34
## kappa Serpentids (KSE)              : 2
## eta Aquariids (ETA)                 : 1900
## Northern iota Aquariids (NIA)       : 80
## Corvids (COR)                       : 6
## Southern mu Sagittariids (SSG)      : 100
## Northern delta Cancrids (NCC)       : 110
## Southern delta Cancrids (SCC)       : 82
## alpha Antliids (AAN)                : 6
## epsilon Aquilids (EAU)              : 4
## Northern June Aquilids (NZC)        : 27
## Southern June Aquilids (SZC)        : 7
## July Pegasids (JPE)                 : 12
## Piscis Austrinids (PAU)             : 24
## July gamma Draconids (GDR)          : 3
## psi Cassiopeiids (PCA)              : 10
## eta Eridanids (ERI)                 : 27
## August Draconids (AUD)              : 14
## Aurigids (AUR)                      : 3
## September epsilon Perseids (SPE)    : 5
## October Capricornids (OCC)          : 3
## alpha Monocerotids (AMO)            : 6
## November Orionids (NOO)             : 178
## Phoenicids (PHO)                    : 1
## Southern chi Orionids (ORS)         : 146
## January Leonids (JLE)               : 35
## omega Serpentids (OSE)              : 10
## theta Coronae Borealids (TCB)       : 17
## lambda Bootids (LBO)                : 24
## xi Coronae Borealids (XCB)          : 10
## epsilon Perseids (EPR)              : 8
## epsilon Pegasids (EPG)              : 15
## beta Equuleids (BEQ)                : 17
## alpha Lacertids (ALA)               : 14
## sigma Serpentids (SSE)              : 7
## alpha Hydrids (AHY)                 : 17
## October Ursae Majorids (OCU)        : 8
## December alpha Draconids (DAD)      : 73
## December chi Virginids (XVI)        : 27
## December kappa Draconids (DKD)      : 20
## nu Eridanids (NUE)                  : 11
## omicron Eridanids (OER)             : 16
## psi Ursae Majorids (PSU)            : 20
## h Virginids (HVI)                   : 42
## x Herculids (XHE)                   : 8
## June mu Cassiopeiids (JMC)          : 13
## phi Piscids (PPS)                   : 65
## chi Taurids (CTA)                   : 97
## November theta Aurigids (THA)       : 160
## c Andromedids (CAN)                 : 11
## December sigma Virginids (DSV)      : 21
## June iota Pegasids (JIP)            : 1
## kappa Ursae Majorids (KUM)          : 2
## December phi Cassiopeiids (DPC)     : 9
## February epsilon Virginids (FEV)    : 15
## rho Puppids (RPU)                   : 1
## lambda Ursae Majorids (LUM)         : 6
## Southern lambda Draconids (SLD)     : 2
## eta Hydrids (EHY)                   : 51
## July xi Arietids (JXA)              : 37
## 49 Andromedids (FAN)                : 17
## Sporadic                            : 150080
