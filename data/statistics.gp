#!/usr/bin/gnuplot
reset
set terminal pdfcairo size 16cm,20cm font 'Ubuntu,16'
set output '../paper/figs/meteor_statistics.pdf'
set encoding iso
set datafile separator ','


iccd='<gunzip -c mu+iccd_main.csv.gz'
tomoe='<gunzip -c mu+tomoe_main.csv.gz'

set bar 0
set lmargin 7.5
set bmargin 0
set tmargin 0
set rmargin 2

set print 'stats_vmag_iccd.txt'
stats iccd u 75
set print 'stats_vmag_tomoe.txt'
stats tomoe u 141
unset print

set multiplot
set size 1,0.445

set xr [0.3:11.7]
set xtics 0,1,12 format ''
set ytics format '%4.0f'
set key Left left rev samplen 2

set origin 0,0.545
unset xlabel
set ylabel 'Meteor Altitude (km)' font ',20' offset -0.5,0
set yr [73:128]
set ytics auto
plot \
  iccd u 75:23 w p not lc 1 pt 6 ps 0.5, \
  tomoe u 141:95 w p not lc 7 pt 4 ps 0.5, \
  iccd u 75:24 w p not lc 1 pt 7 ps 0.3, \
  tomoe u 141:96 w p not lc 7 pt 5 ps 0.3, \
  iccd u 75:23:(0):($24-$23) w vec nohead lc 1 not,\
  tomoe u 141:95:(0):($96-$95) w vec nohead lc 7 not,\
  iccd u 75:(0) w lp lc 1 pt 6  t 'MU + CCD', \
  tomoe u 141:(0) w lp lc 7 pt 4 t 'MU + Tomo-e'

set xtics 0,1,12 format '%.0f'
set origin 0,0.09
set xlabel 'Absolute Magnitude in the V-band' font ',20'
set ylabel 'Geocentric Velocity (km&{,}s^{-1})' font ',20'
set yr [0:90]
set ytics 0,10,80
plot \
  iccd u 75:20 w p pt 6 lc 1 ps 0.5 not, \
  tomoe u 141:91 w p pt 4 lc 7 ps 0.5 not, \
  iccd u 75:(-1) w p lc 1 pt 6  t 'MU + CCD', \
  tomoe u 141:(-1) w p lc 7 pt 4 t 'MU + Tomo-e'

unset multiplot
