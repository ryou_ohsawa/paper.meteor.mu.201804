function [lf_data, mf_data] = generate_functions(
                                  database, Niter=10, Nmax=1000, mode=1)
  ## Vini: column 19
  ## RCS : column 20
  ## SNR : column 21
  hours = 845.8;
  rates = 1/845.8;
  rcs = database(:,20);
  [rcs_sorted,sort_idx] = sort(rcs, 'descend');
  database_sorted   = database(sort_idx,:);
  vini_sorted = database_sorted(:,19);
  snr_sorted  = database_sorted(:,21);
  mag_sorted  = -0.1693604*rcs_sorted+4.4259027;
  radz_sorted = radiant_zenith(database_sorted, 1e7);
  area_unc    = pi*(tand(3.0)*100)^2*ones(size(rcs_sorted));
  flux_unc    = ones(size(rcs_sorted))*rates./area_unc;
  ## array collection loaded
  load('./collection_area.txt.gz');
  bins  = collection(:,1)+mean(diff(collection(:,1)))/2.0;
  area = collection(:,2);
  area_sorted = interp1(bins,area,rcs_sorted,'pchip','EXTRAP');
  ##
  ## compile LF_DATA
  flux_sorted = rates./area_sorted;
  fluxerr_sorted = cumstd_simulation(rcs_sorted, flux_sorted, Niter, Nmax);
  lf_data = [ rcs_sorted, mag_sorted, area_unc, area_sorted, ...
              cumsum([flux_unc, flux_sorted]), ...
              fluxerr_sorted, snr_sorted ];
  ##
  ## compile MF_DATA
  if (mode==1)
    logmass = 6.31-0.40*mag_sorted -3.92*log10(vini_sorted) ...
              -0.41*log10(cosd(radz_sorted));
  else
    dmag = 2.82873*log10(cosd(radz_sorted))+0.42491;
    dmag = dmag + 2.5*log10(0.453*0.28*0.63768);
    logmass = 1.8765-2.31390*log10(vini_sorted)-0.38153*(mag_sorted+dmag);
  endif
  [mass_sorted,sort_idx] = sort(10.^logmass, 'descend');
  rcs_sorted  = rcs_sorted(sort_idx);
  mag_sorted  = mag_sorted(sort_idx);
  area_sorted = area_sorted(sort_idx);
  flux_sorted = flux_sorted(sort_idx);
  snr_sorted  = snr_sorted(sort_idx);
  vini_sorted = vini_sorted(sort_idx);
  radz_sorted = radz_sorted(sort_idx);
  fluxerr_sorted = cumstd_simulation(mass_sorted, flux_sorted, Niter, Nmax);
  mf_data = [ mass_sorted, logmass(sort_idx), area_unc, area_sorted, ...
              cumsum([flux_unc, flux_sorted]), fluxerr_sorted, ...
              snr_sorted, vini_sorted, radz_sorted ];
endfunction
