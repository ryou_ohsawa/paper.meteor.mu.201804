#!/usr/bin/env python
from argparse import ArgumentParser as ap
import pandas as pd
import numpy as np


if __name__ == '__main__':
  parser = ap(description='identify major meteor showers by Dsh-criterion')
  parser.add_argument(
    'database', type=str, help='meteor orbital elements catalog')
  parser.add_argument(
    'observation', type=str, help='meteor observations')

  parser.add_argument(
    '-t', '--threshold', action='store', type=float, default=0.2,
    help='threshold value for Dsh-criterion.')
  parser.add_argument(
    '-o', '--output', action='store', type=str,
    help='output meteor files without shower contributions.')


  args = parser.parse_args()

  cat = pd.read_csv(args.database)
  df = pd.read_csv(args.observation)
  col = df.columns

  a_obs = df['Semimajor_axis_(AU)']
  q_obs = df['Perihelion_dist_(AU)']
  i_obs = np.deg2rad(df['Inclination_(deg)'])
  o_obs = np.deg2rad(df['Argument_of_periapsis_(deg)'])
  O_obs = np.deg2rad(df['Lon_of_asc_node_(deg)'])
  e_obs = df['Eccentricity']

  n_showers = cat.shape[0]
  Dsh_array = []
  for n in range(n_showers):
    if 'Daytime' in cat.name[n]: continue
    a,q,e,o,O,i = cat.loc[n,['a','q','e','peri','node','incl']]
    o,O,i = [np.deg2rad(x) for x in (o,O,i)]
    I21 = np.arccos(np.clip(
      np.cos(i)*np.cos(i_obs)+np.sin(i)*np.sin(i_obs)*np.cos(O_obs-O),-1,1))
    gamma = np.sign(180.0-abs(O_obs-O))
    P21 = o_obs-o + 2*gamma*np.arcsin(np.clip(
      np.cos((i_obs+i)/2.0)*np.sin((O_obs-O)/2.0)/np.cos(I21/2.0),-1,1))
    Dsh = np.sqrt( (e_obs - e)**2 + (q_obs - q)**2 +
      (2*np.sin(I21/2.0))**2+((e_obs + e)*np.sin(P21/2.0))**2)
    Dsh_array.append(Dsh)
    t = (Dsh < args.threshold).sum()
    if t > 0:
      print('## {:36s}: {}'.format(
        '{} ({})'.format(cat.name[n],cat.code[n]),t))
  Dsh = np.array(Dsh_array).T
  c = (Dsh < args.threshold).any(axis=1)

  df = df.loc[~c]
  if args.output:
    df.to_csv(args.output, index=False, header=True)
  print('## {:36s}: {}'.format('Sporadic', df.shape[0]))
