function [lc,M] = lightcurve(m,y)
  ## Display light curve and return the brightest magnitude
  ##
  ## Parameters:
  ##   m: array of meteoroid mass (kg)
  ##   y: array of [altitude (m), velocity (m/s), temperature (K)]
  ##
  ## Return:
  ##   M: the brightest magnitude
  ##
  dmdt = -diff(m)./diff(y(:,4));      ## dm/dt
  hc = (y(1:end-1,1)+y(2:end,1))/2.0; ## altitude [m]
  vc = (y(1:end-1,2)+y(2:end,2))/2.0; ## velocity [m/s]
  I = dmdt.*lumieff(vc)/2.0.*vc.^2;     ## emitted energy [W]
  ## zeropoint flux in the V-band in untis of W/m2
  mag0 = -2.5*log10(363.1e-4*0.085e-6);
  ## absolute magnitude at the distance of 100 km
  mag  = -real(2.5*log10(I./(4*pi*100e3.^2))) - mag0;
  [M,xi] = min(mag);
  plot(hc, mag, '-');
  set(gca(),'xdir','reverse');
  set(gca(),'ydir','reverse');
  lc = [hc, mag, vc];
endfunction

function em = lumieff(v)
  ## Luminous Efficiency Factor
  ##
  ## Parameters:
  ##     v: velocity (m/s)
  ##
  ## ref: Hill et al., 2005, A&A, 444, 615
  epsmu = 7.668e6;       # [J/kg]
  epsmu *= 0.453 * 0.28; # scaling by Weryk and Brown (2013)
  em0 = ((-2.1887e-9)*v.^2+(4.2903e-13)*v.^3+(-1.2447e-17)*v.^4);
  em1 = (0.01333*(v/1e3).^(1.25));
  em  = 2*epsmu*(em0.*(v<20e3) + em1.*(v>=20e3))./v.^2;
endfunction
