#!/usr/bin/gnuplot
reset
set terminal pdfcairo size 16cm,16cm font 'Ubuntu,16'
set output '../paper/figs/cumulative_rcs_function.pdf'
set encoding iso

mu='luminosity_function.dat'

set bar 0
set lmargin 7.5
set bmargin 2
set tmargin 0
set rmargin 2

unset log x
set log y

set multiplot
set size 1.0,0.7
set origin 0,0.3

set mxtics 4
set mytics 10

set xr [54:-50]
set yr [2e-7:5e+1]
set xtics format ''
set ytics auto format ' 10^{%L}'
unset xlabel
set ylabel 'Cumulative Number Flux (h^{-1}&{,}km^{-2})' font ',20'
set tics front

set label 1 '(a)' left at graph 0, graph 1 \
font 'Ubuntu-Bold,22' offset 1,-1

plot \
  mu u 1:5 w l dt (16,8) \
  t 'RCS with constant collecting area', \
  mu u 1:($6+3*$7):($6-3*$7) w filledc lc 7 fs solid 0.1 not, \
  mu u 1:($6+$7):($6-$7) w filledc lc 7 fs solid 0.3 not, \
  mu u 1:6 w l lw 2 lc 7 \
  t 'RCS with collecting area correction'

set size 1.0,0.3
set origin 0,0.04
set auto y
set yr [2e1:2e4]
set xtics format '%.0f'
set ytics 1e-8,10,1e+4 format ' 10^{%-2L}'
set xlabel 'RCS at the maximum SNR (dBsm)' font ',20'
set ylabel 'Area (km^{2})' font ',20'

set label 1 '(b)'

plot \
  mu u 1:3 w l dt (16,8) \
  t '6{\272}-diameter circle at 100&{,}km', \
  mu u 1:4 w l lw 2 lc 7 \
  t 'Estimated collecting area'
unset multiplot
