#!/usr/bin/octave

function [q,bins] = collection_area(data_array,N=25,P=50)
  pkg load nan
  cosd = @(x) cos(pi*x/180.);
  sind = @(x) sin(pi*x/180.);
  hx = @(h,a,z) h.*cosd(a).*sind(z);
  hy = @(h,a,z) h.*sind(a).*sind(z);
  hz = @(h,a,z) h.*cosd(z);
  vx = @(h1,a1,z1,h2,a2,z2,t) ...
     hx(h1,a1,z1)+t.*(hx(h2,a2,z2)-hx(h1,a1,z1));
  vy = @(h1,a1,z1,h2,a2,z2,t) ...
    hy(h1,a1,z1)+t.*(hy(h2,a2,z2)-hy(h1,a1,z1));
  t = @(h1,a1,z1,h2,a2,z2) ...
   (100.0-hz(h1,a1,z1))./(hz(h2,a2,z2)-hz(h1,a1,z1));
  tx = @(h1,a1,z1,h2,a2,z2) ...
    vx(h1,a1,z1,h2,a2,z2,t(h1,a1,z1,h2,a2,z2));
  ty = @(h1,a1,z1,h2,a2,z2) ...
    vy(h1,a1,z1,h2,a2,z2,t(h1,a1,z1,h2,a2,z2));
  tr = @(h1,a1,z1,h2,a2,z2) ...
    sqrt(tx(h1,a1,z1,h2,a2,z2).^2+ty(h1,a1,z1,h2,a2,z2).^2);
  v1 = data_array(:,[22,24,25]);
  v2 = data_array(:,[23,26,27]);
  r = tr(v1(:,1),v1(:,2),v1(:,3),v2(:,1),v2(:,2),v2(:,3));
  rcs = data_array(:,20);
  area = pi*r.^2;
  bins = linspace(min(rcs),max(rcs)+1,N)';
  [m,idx] = histc(rcs, bins);
  for n=1:numel(bins)
    target(n,1) = percentile(area(idx==n),P);
  endfor
  func = @(p) sumsq((target-cumsum(p)));
  p = zeros(N,1); p(1) = 100;
  [p,obj,info,iter] = sqp(p, func, [], [], eps(N,1), inf(N,1));
  q = cumsum(p);
endfunction
