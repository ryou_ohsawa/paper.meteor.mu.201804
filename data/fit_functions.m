function [pl,pm] = fit_functions(l,m, mlim=[3.0,8.5])
  nrm = interp1(l(:,2), l(:,6), 6.0);
  idx = mlim(1)<l(:,2) & l(:,2)<mlim(2);
  x = l(idx,2); y = log10(l(idx,6)/nrm);
  pl = polyfit(x,y,1);
  semilogy(l(:,2), l(:,6)/nrm, '.-', 'markersize', 2.0, ...
           l(:,2), 10.^polyval(pl,l(:,2)), '-');
  pl = 10.^pl;
endfunction
