#!/usr/bin/gnuplot
reset
set terminal pdfcairo size 16cm,11cm font 'Ubuntu,16'
set output '../paper/figs/rcs_against_magnitude.pdf'
set encoding iso
set datafile separator ','


iccd='<gunzip -c mu+iccd_main.csv.gz'
tomoe='<gunzip -c mu+tomoe_main.csv.gz'
tablename = 'rcs_optical_full.txt'

sn2err(x) = 20.*log10(1.+10**(-x/20.))
wrap(x) = x>360.?wrap(x-360.):x<0.?wrap(x+360.):x
set bar 0

set lmargin 7
set bmargin -1
set tmargin -1
set rmargin 2
set xr [-42:22]
set xtics -40,10,20

## linear regression obtained by simulation
# slope     = -0.169 +/- 0.006
# intercept =  4.426 +/- 0.126
func(x) = -0.1693604*x + 4.4259027
func_p(x) = (-0.1693604+0.0059046)*x + (4.4259027+0.1258058)
func_m(x) = (-0.1693604-0.0059046)*x + (4.4259027-0.1258058)

set key left Left rev samplen 3
set yr [12:0]
set ytics 0,2,20
set xlabel 'RCS at the maximum SNR (dBsm)' font ',20'
set ylabel 'Absolute Magnitude in the V-band' font ',20'
plot \
  iccd u 21:75:(sn2err($22)):76 w xyerr pt 6 \
  t 'MU + CCD', \
  tomoe u 93:141:(sn2err($94)):142 w xyerr pt 4 lc 7 \
  t 'MU + Tomo-e', \
  [-40:20] func(x) lw 2 lc 6 dt (32,16) \
  t 'Linear Regression'

## Obsolete Linear Regression Line by A. Hirota
# [-40:20] -x/4.58+16.91/4.58 lw 2 lc 6 dt (32,16) \
# t 'Linear Regression (Hirota)'
