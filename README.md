## Call for manuscripts to the Meteoroids

- Submission open on August 31, 2019
- Submission deadline is ~~November 1~~ November 15, 2019
- Revision deadline is March 4, 2020
- Acceptance deadline is May 30, 2020

## Referees' comments
- [Comment 1 in 2020-02-06](paper/review/reviewer_comments_PSS_20200206a.pdf)
- [Comment 2 in 2020-02-06](paper/review/reviewer_comments_PSS_20200206b.pdf)
