\subsection{The relationship between the RCS and the optical magnitude}
Figure~\ref{fig:results:rcsmag} indicates that the data follow a single and linear relationship. No apparent deviation from the regression line is confirmed. The dependence of the relationship on the meteor speed is investigated by splitting the sample into the fast (${>}\,45\,\mathrm{km\,s^{-1}}$) and slow (${\leq}\,45\,\mathrm{km\,s^{-1}}$) members, but no significant difference is confirmed. This suggests that sporadic meteors from the apex, antihelion, and north toroidal sources follow the same relationship.

The scatter in Figure~\ref{fig:results:rcsmag} is much larger than the errors of the data. \revision{0206}{This may in part be due to the fact that the sections where the RCS and the optical magnitude were measured were close but not exactly the same. Both the RCS and the optical brightness are variable along the trajectory \citep[e.g., \textit{see} Figure~11 in][]{brown_simultaneous_2017}. The variation in the RCS and the optical brightness should contribute to the scatter. Especially, fragmentation may cause a sudden increase in the RCS and optical brightness, but the impacts of the fragmentation on the RCS and the magnitude are not necessarily the same \citep{campbell-brown_high-resolution_2013,brown_simultaneous_2017}. Fragmentation may cause pulsations in head echo RCS curves due to interference from two or more scattering centers \citep{kero_three-dimensional_2008}, while the luminosity produced is assumed to be proportional to the total kinetic energy lost by the meteoroid \citep{campbell-brown_high-resolution_2013}, thus proportional to the increased cross-sectional area to mass of the fragments. \citet{weryk_canadian_2013} reported that 17\% of meteors detected by the CAMO system showed clear signs of fragmentation. Similarly, a significant fraction of the data in Figure~\ref{fig:results:rcsmag} could be affected by fragmentation. This may partly explain the large scatter: ${\sim}2\,\mathrm{mag}$ in the optical magnitude and ${\sim}10\,\mathrm{dBsm}$ in the RCS, which are roughly consistent with the changes caused by the fragmentation.} Several studies have suggested that the ratio of the emission to ionization coefficients depends on the velocity \citep{saidov_luminous_1989,jones_theoretical_1997,weryk_simultaneous_2013}. This dependence may contribute to the scatter, but the difference in the distributions was not confirmed in the present data. A variety of chemical compositions may contribute to the scatter. Spectroscopic observations are required to confirm it.

Although the origin of the large scatter has not been identified, we tentatively conclude that the relationship between the RCS and the optical magnitude is well approximated by a linear function over a magnitude range of about $1$--$9\,\mathrm{mag}$, but we do not exclude the possibility that any deviations from the linear relationship are hidden within the scatter. \revision{0206}{The possibility that the relationship is variable or there are multiple relationships is not excluded as well. The scatter can be attributed to possible variations in the relationship. In such a case, the present relationship is considered to be an averaged one over the current dataset. Note that the uncertainties in Equation~(\ref{eq:results:rcsmag}) were derived with the assumption that the relationship is unique among the dataset, where the variation in the relationship was not taken into account.}

\revision{0206}{A scattering model of a head echo plasma was developed by \citet{close_technique_2004}, which provided a method to estimate the meteoroid mass, referred as to \textit{scattering mass}, from the head echo RCS \citep{close_new_2005}. \citet{close_meteor_2007} investigated the dependence of the head echo RCS on the electron line density ($q$), the scattering mass ($m_\mathrm{s}$), the velocity ($v$), and the mean-free-path ($l$). A multivariate regression provided a relationship $q \propto m^{1.08} v^{3.09} l^{-0.11}$ and the head echo RCS ($A$) was approximated as $A \propto q^{1.05} \propto m_\mathrm{s}^{1.05{\times}1.08}$. Here, we simply assume that the brightness of the meteor approximately follows $I \propto m_\mathrm{p}$, where $m_\mathrm{p}$ is the photometric mass. By equating $m_\mathrm{s}$ with $m_\mathrm{p}$, a relationship between the magnitude ($M_V$) and the head echo RCS is derived: $M_{V} = -0.22A+\text{const.}$ The slope of this relationship is similar to but steeper than the present result. This may implicate that the dependence of the head echo RCS is larger than suggested in \citet{close_meteor_2007}, or that the dependence of the optical brightness on the meteoroid mass is smaller. Note that the comparison above is highly simplified. A model calculation which evaluates the head echo RCS and the optical brightness simultaneously is required.}

\citet{nishimura_high_2001} provided a conversion function from the optical magnitude to the radar received power in units of $\mathrm{dB}$, where the optical magnitude decreased by about $0.3\,\mathrm{mag}$ when the radar power increased by $1\,\mathrm{dB}$, corresponding to the slope of $-0.3$. Since their result was derived from 20 simultaneous meteors, the slope could be affected by a considerable uncertainty. We estimate the uncertainty of the slope in case that the sample size is limited to 20 using a bootstrap sampling method: 20 meteors are randomly selected from the present 332 simultaneous meteors and a slope is derived by a least-square method. This process is repeated 1,000 times. Then, the posterior probability distribution of the slope is approximated by the distribution of the derived slopes. The 95\% confidence interval is $(-0.13, -0.32)$. Thus, we presume that the present slope is marginally consistent with that in \citet{nishimura_high_2001}. \citet{brown_simultaneous_2017} presented the relationship between the RCS the optical magnitude based on their 105 simultaneous meteors (\textit{see}, Figure~9). While no regression line was presented, the optical magnitude decreased roughly by $0.05$--$0.16\,\mathrm{mag}$ when the RCS increased by $1\,\mathrm{dB}$. The slope seems smaller than that of the present result. The optical magnitudes ranged over roughly $0$--$6\,\mathrm{mag}$ in \citet{brown_simultaneous_2017}, while only a handful of meteors were detected in this magnitude range in the present work. The possibility that the relationship changes around $5\,\mathrm{mag}$ is not excluded.

The data of \runA and \runB were obtained using different systems and on different days. Equation~(\ref{eq:results:rcsmag}) could suffer from systematic errors, such as the difference in spectral response and the annual variations. Further observations are required to evaluate the uncertainty of Equation~(\ref{eq:results:rcsmag}). The data of \runB were obtained in only four nights. This suggests that the combination of the MU radar and Tomo-e Gozen is promising to investigate the annual and diurnal variations in the relationship between the RCS and the optical magnitude.


\subsection{Meteor Luminosity Function of the MU radar Meteor Head Echo Database}
A luminosity function of visible meteors has been widely approximated by \revision{0206}{a power-law} function \citep{hawkins_influx_1958}:
\begin{equation}
  \label{eq:discussion:lf}
  \log_{10}N({<}M) = \log_{10}N_0 + M\log_{10}r,
\end{equation}
where $N({<}M)$ and $N_0$ are the event rates of meteors brighter than $M$-th and zero-th magnitudes, respectively, and $r$ defines the slope of the distribution, generally referred as to \textit{the population index}. \citet{cook_flux_1980} suggested that the luminosity function was well approximated by Equation~(\ref{eq:discussion:lf}) from $-2.4$ to $12\,\mathrm{mag}$. Here, Equation~(\ref{eq:results:rcsmag}) is applied to the data collected with the MU radar from 2009 to 2015, and the luminosity function of the meteors detected by the MU radar is investigated. The data consist of 157043 meteors in total. In the following discussion, we assume that the contributions from meteor showers are negligible.

\begin{figure*}
  \centering
  \includegraphics[width=0.495\linewidth]{figs/cumulative_rcs_function.pdf}
  \includegraphics[width=0.495\linewidth]{figs/luminosity_function.pdf}
  \caption{The luminosity function of sporadic meteors. Panel (a) shows the cumulative number flux against the RCS. The black dashed line indicates the number flux assuming that the meteor collecting area does not depend on the RCS. The number flux shown by the red line properly takes into account the dependency. The shaded regions indicate $1\sigma$- and $3\sigma$-level uncertainties. The dependency of the meteor collecting area on the RCS is illustrated in Panel (b) in the red line. The black dashed line is the constant collecting area for reference. Panel (c) shows the luminosity function of sporadic meteors. The uncertainties are the same as in Panel (a). A regression curve is shown by the blue dashed line.}
  \label{fig:discussion:luminosity}
\end{figure*}

Panel (a) of Figure~\ref{fig:discussion:luminosity} illustrates the cumulative number flux against the RCS in units of $\mathrm{h^{-1}\,km^{-2}}$. The black dashed line is the distribution assuming that the meteor collecting area is uniformly $86.3\,\mathrm{km^2}$ (a disk of $6\degree$ in diameter at $100\,\mathrm{km}$ in altitude). The meteor collecting area of the MU radar, however, depends on the RCS, since bright meteors can be detected in the side lobes of the beam \citep{kero_first_2011}. The dependence of the collecting area on the RCS is derived as follows; The distance from the beam center at $100\,\mathrm{km}$ ($R_{100\,\mathrm{km}}$) is derived for each meteor by interpolating or extrapolating the trajectory. The area of ${\pi}R_{100\,\mathrm{km}}^2$ is calculated. The data are divided into groups in the RCS and the median of the area among each group is adopted as the collecting area as a function of the RCS. The derived collecting area against the RCS is shown in Panel (b) of Figure~\ref{fig:discussion:luminosity}. The cumulative number flux calculated based on the derived collecting area is shown by the red line with the $1\sigma$- and $3\sigma$-uncertainty regions in Figure~\ref{fig:discussion:luminosity}. The cumulative number flux larger than $25\,\mathrm{dBsm}$ is highly uncertain. The cumulative number flux peaks out around ${-}25\,\mathrm{dBsm}$ simply due to the detection limit. In a range from $-20$ to $20\,\mathrm{dBsm}$, the cumulative number flux seems well approximated by a linear function.

The cumulative number flux against the RCS is converted into the luminosity function by applying Equation~(\ref{eq:results:rcsmag}). Panel (c) of Figure~\ref{fig:discussion:luminosity} illustrates the luminosity function in the red line with the $1\sigma$- and $3\sigma$-uncertainty regions. The luminosity function basically follows an exponential law, which is consistent with previous works \citep[e.g.,][]{hawkins_influx_1958,hawkes_television_1975,cook_flux_1980,ohsawa_luminosity_2019}. The detection limit corresponds to about $10\,\mathrm{mag}$, which is roughly consistent with the detection limit for EISCAT in \citet{pellinen-wannberg_meteor_1998}. The population index is derived as $r = 3.52{\pm}0.12$ by fitting a linear function between $-1.5$ and $9.5\,\mathrm{mag}$, taking into account the systematic uncertainty from Equation~(\ref{eq:results:rcsmag}).

\begin{table*}
  \centering
  \caption{The population indexes in literature}
  \label{tab:discussion:index}
  \begin{tabular}{llll}
    \toprule
    \multicolumn{1}{c}{Reference}
    & \multicolumn{1}{c}{$r$-index}
    & \multicolumn{1}{c}{$s$-index}
    & \multicolumn{1}{c}{Comment} \\\midrule
    \citet{hawkins_influx_1958}
    & ${\sim}3.45$
    & ${\sim}2.34$
    & $-2$--$3\,\mathrm{mag}$, photographic \\
    \citet{kresakova_magnitude_1966}
    & $\phantom{\sim}3.5$
    & $\phantom{\sim}2.35$
    & $-4$--$6\,\mathrm{mag}$, 21996 visual meteors \\
    \citet{hughes_diurnal_1972}
    & \multicolumn{1}{c}{---}
    & $\phantom{\sim}2.04{\pm}0.04$
    & 30000 HF radar echoes \\
    \citet{clifton_television_1973}
    & ${\sim}3.17$
    & ${\sim}2.252$
    & $7$--$11\,\mathrm{mag}$, TV observation\\
    \citet{hughes_influx_1974}
    & $\phantom{\sim}3.73{\pm}0.07$
    & $\phantom{\sim}2.43{\pm}0.02$
    & $-6$--$0\,\mathrm{mag}$, 10287 visual meteors \\
    \citet{hawkes_television_1975}
    & \multicolumn{1}{c}{---}
    & $\phantom{\sim}2.02{\pm}0.04$
    & $3$--$7\,\mathrm{mag}$, TV observation \\
    \citet{stohl_magnitude_1976}
    & $\phantom{\sim}3.70$
    & \multicolumn{1}{c}{---}
    & 12867 visual meteors \\
    \citet{cook_flux_1980}
    & $\phantom{\sim}3.41$
    & $\phantom{\sim}2.335$
    & $7$--$12\,\mathrm{mag}$, phototubes \\
    \citet{rendtel_population_2004}
    & $\phantom{\sim}2.95{\pm}0.06$
    & $\phantom{\sim}2.17{\pm}0.03$
    & 301499 visual meteors, IMO VMDB \\
    \citet{ohsawa_luminosity_2019}
    & $\phantom{\sim}3.1{\pm}0.4$
    & \multicolumn{1}{c}{---}
    & $3$--$9\,\mathrm{mag}$, video-rate magnitude \\
    The present result
    & $\phantom{\sim}3.52{\pm}0.12$
    & $\phantom{\sim}2.46{\pm}0.09$
    & $0$--$9\,\mathrm{mag}$, MURMHED \\
    \bottomrule
  \end{tabular}
\end{table*}


\begin{figure*}
  \centering
  \begin{minipage}[b]{0.49\linewidth}
    \includegraphics[width=\linewidth]{figs/luminosity_function_monthly.pdf}
    \includegraphics[width=\linewidth]{figs/population_index_monthly.pdf}
  \end{minipage}
  \hspace{0.5em}%
  \begin{minipage}[b]{0.49\linewidth}
    \includegraphics[width=\linewidth]{figs/luminosity_function_hourly.pdf}
    \includegraphics[width=\linewidth]{figs/population_index_hourly.pdf}
  \end{minipage}
  \caption{Panel (a-1) shows the month-by-month variation in the luminosity functions, while the diurnal variation in the luminosity functions is shown in Panel (b-1). All the luminosity functions are normalized around $5\,\mathrm{mag}$. The numbers in the legends of each panel indicate the population indexes. The annual and diurnal variations in the population index are shown in Panels (a-2) and (b-2), respectively. The times are based on the detection time stamps in the local time (Japan Standard Time).}
  \label{fig:discussion:monthly-and-hourly}
\end{figure*}

Table~\ref{tab:discussion:index} lists some population indexes reported in literature. A more detailed overview found in \citet{rendtel_population_2004}. The population indexes have been measured in different methods and different magnitude ranges. As shown in Table~\ref{tab:discussion:index}, there is a considerable variation in the reported population indexes. The present result is roughly consistent with \citet{hawkins_variation_1956}, \citet{kresakova_magnitude_1966}, \citet{cook_flux_1980}, and \citet{ohsawa_luminosity_2019}.

Several studies have pointed out the annual and diurnal variations in the population index \citep[e.g.,][]{hughes_diurnal_1972,rendtel_population_2004}. Panel (a-1) of Figure~\ref{fig:discussion:monthly-and-hourly} illustrates the annual variation in the luminosity function. The luminosity functions are normalized around $5\,\mathrm{mag}$. The annual average of the population index is about 3.49 with a standard deviation of 0.11. Panel (a-2) shows a possible annual variation in the population index: it tends to be high in \revision{0206}{July--September} and low in \revision{0206}{December--March}. \citet{rendtel_population_2004}, however, suggested that the population index increases (decreases) in winter (summer) in the northern hemisphere, which is the opposite to the present result. A similar trend was also reported by \citet{stohl_magnitude_1976}. \citet{rendtel_population_2004} complied the dataset from observations in 1998--2003 and the observations in \citet{stohl_magnitude_1976} were in 1944--1950. The present data were collected 2009--2015. This could be explained in case that the trend in the population index changed in the timescale of decades. Limiting magnitudes in \citet{stohl_magnitude_1976} and \citet{rendtel_population_2004} were about $4$ and $6\,\mathrm{mag}$, respectively. Instead, the present result is biased toward meteors in a magnitude range of $4$--$8\,\mathrm{mag}$. This humbly implies that brighter and fainter parts of the luminosity function show different annual variations.

Panel (b-1) of Figure~\ref{fig:discussion:monthly-and-hourly} shows the diurnal variation in the luminosity function, normalized as in Panel (a-1). All the times are in the local time (Japan Standard Time). Panel (b-2) shows a diurnal variation in the population index. The averaged population index is 3.33 with a standard deviation of 0.21, suggesting the diurnal variation is more significant than the annual variation. The population index almost constantly decreases \revision{0206}{from 21:00 and reaches a minimum around 09:00}. This trend is roughly consistent with \citet{hughes_diurnal_1972} and \citet{rendtel_population_2004}, although \citet{stohl_magnitude_1976} reported an almost constant population index in this period. The present result suggests that the diurnal variation in the luminosity function does not depend on the optical magnitude, or the meteoroid mass.

Note that the present population index is heavily dependent on Equation~(\ref{eq:results:rcsmag}). In the discussion above, we naively assume that the relationship between the RCS and the magnitude is constant. The results could be affected by any possible annual or diurnal variations in the relationship. \revision{0206}{Figure~\ref{fig:results:radiant} indicates that a large part of the meteors simultaneously detected by radar and optically were attributed to the antihelion and north toroidal sources. The meteors from these sources are typically slower than those from the apex sources, which are dominant in the radar observation \citep{kero_2009-2010_2012}. Thus, the meteors which were used to derive Equation~(\ref{eq:results:rcsmag}) could be biased toward a larger part of the meteor population. Part of the diurnal variation can be attributable to such an observational bias.} More systematic observations are required to examine the validity of the present results.

\begin{figure*}
  \centering
  \includegraphics[width=0.9\linewidth]{figs/mass_function.pdf}
  \caption{The mass function of sporadic meteors is shown by the red line. The $1\sigma$- and $3\sigma$-level uncertainties are shown by the shaded regions. The blue dashed line indicates the regression curve.}
  \label{fig:discussion:mass}
\end{figure*}

\revision{0206}{The optical magnitude is converted into mass using the ablation equations \citep[e.g.,][]{hawkes_quantitative_1975,ceplecha_meteor_1998}. When a meteoroid enters the atmosphere, it is decelerated and heated through interactions with atmospheric constitutions. The meteoroid loses its kinetic energy by decreasing bot its mass and velocity. Part of the lost energy is converted into light and observed in optical. The fraction of the meteoroid's kinetic energy which is converted into light is called the luminous efficiency. The thermal ablation of a meteoroid is calculated following the procedure given by \citet{hill_high_2005}. The parameters in the ablation equations are adopted from \citet{hill_high_2005}. A typical molecular mass and bulk density are respectively set to $50\,amu$ and $3300\,\mathrm{kg\,m^{-3}}$. The calculation ends when the meteoroid loses 99.999\% of the initial mass. The brightest magnitudes are calculated using the normal luminous equation for different meteoroid masses ($10^{1},\,10^{0},\,\ldots,\,10^{-7}\,\mathrm{g}$), radiant zenith angles ($0,\,30\degree,\,45\degree,\,60\degree$, and $90\degree$), and initial velocities ($10,\,20,\,\ldots,\,70\,\mathrm{km/s}$). The luminous efficiency is taken from \citet{hill_high_2005}, but scaled by 0.081\footnote{The scaling factor in \citet{weryk_simultaneous_2013} is $0.453{\times}0.28$, which was optimized to the observation in the \textit{R}-band. It is further multiplied by 0.638, which is the band width ration of \textit{V}-band to the \textit{R}-band.} as described in \citet{weryk_simultaneous_2013}. Then, a relationship to calculate the meteoroid mass from the magnitude, velocity, and radiant zenith angle is approximated by the following function:}
\begin{equation}
  \label{eq:discussion:mass:h05}
  \revision{0206}{
    \log_{10}m = 2.76-0.38M_V-2.31\log_{10}V_{\infty}-1.07\log_{10}\cos{z},
  }
\end{equation}
\revision{0206}{where $m$ is the mass in units of $\mathrm{g}$, $M_V$ is the optical magnitude, $V_\infty$ is the incident velocity in units of $\mathrm{km\,s^{-1}}$, and $z$ is the zenith angle of the radiant point. $V_\infty$ and $z$ are calculated from the trajectories measured by the MU radar. The deviation from Equation~(\ref{eq:discussion:mass:h05}) is typically up to about $0.2\,\mathrm{dex}$. The derived mass depends on the selection of a luminous coefficient. While the current calculation uses the scaled luminous coefficient of \citet{hill_high_2005}, larger luminous coefficients were reported by \citet{ceplecha_fragmentation_2005} and \citet{weryk_simultaneous_2013}. If we adopt those luminous coefficients, the derived mass could be smaller by about an order of magnitude.}

The cumulative mass distribution, or the mass function, is illustrated in Figure~\ref{fig:discussion:mass}. \revision{0206}{The mass function peaks out around $10^{-5}\,\mathrm{g}$, which is attributed to the mass detection limit.} The mass function is expected to follow a power-law function:
\begin{equation}
  \label{eq:discussion:mass_index}
  \log_{10}N({>}m) = \log_{10}N_1 - (s-1)\log_{10}m,
\end{equation}
where $m$ is the meteoroid mass in units of $g$, $N({>}m)$ and $N_1$ are the event rates of meteors larger than $m$ and $1\,\mathrm{g}$, respectively, and $s$ defines the slope of the mass function, referred as to \textit{the mass index}. \revision{0206}{The derived regression curve is shown by the blue line.} Some mass indexes in literature are listed in Table~\ref{tab:discussion:index}. The mass function deviates from the regression curve around $10^{-4}\,\mathrm{g}$. This deviation is likely not a real feature, but could be attributed to the observational bias: the detection limit is given by the RCS rather than the meteoroid mass. \revision{0206}{The derived mass index is $2.46{\pm}0.09$. This value is similar to that in \citet{hughes_influx_1974} and \citet{cook_flux_1980}. The mass flux to the Earth in the mass range of $10^{-5}$--$10^{0}\,\mathrm{g}$ is about $3{\times}10^3\,\mathrm{kg\,day^{-1}}$. \citet{ceplecha_influx_1992} provided an incremental mass flux of interplanetary bodies by compiling several observational researches. By integrating it from $10^{-5}$ to $10^{0}\,\mathrm{g}$, the mass flux was $1{\times}10^3\,\mathrm{kg\,day^{-1}}$, which is roughly coincident with our estimation. \citet{love_direct_1993}, however, estimated the mass flux in the similar mass range to be about $1{\times}10^5\,\mathrm{kg\,day^{-1}}$ from the examinations of impact craters on the Long Duration Exposure Facility satellite. The current estimate is more than an order smaller than their estimate. Note that the estimated mass flux depends on the selection of the luminous efficiency. The mass flux will be smaller if the luminous efficiencies of \citet{ceplecha_fragmentation_2005} and \citet{weryk_simultaneous_2013} are applied.}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End: