\subsection{Simultaneous Meteors of \runA}
We define ``a simultaneous meteor'' as a meteor observed both by radar and optically. In case of \runA, simultaneous meteors were identified based on the trajectories and timings of meteors. When meteors were detected both by radar and optically and the time separation was within 0.5\,s, the meteors were considered as identical. Finally 145 meteors were identified as the simultaneous events.

Some observations were scheduled when meteor showers were active. Meteors belonging to showers were removed based on the D-criterion. The threshold for the D-criterion was set to 0.2 similarly as descried above. The present dataset \revision{0206}{possibly contained 5 Southern Taurids, 13 Geminids, 17 Orionids, 1 Andromedids, 2 December Monocerotids, 1 Comae Berenicids, 2 $\varepsilon$ Geminids, 16 $\eta$ Aquariids, 1 October Capricornids, and 1 November Orionids. The remaining 103 meteors were considered to be sporadic. In the following sections, the 103 sporadic meteors were investigated.}

The brightness of the \runA meteors were calibrated against the $V$-band magnitudes in SKY2000 catalog version 4 \citep{myers_vizier_2001} using UFOAnalyzer. \revision{0206}{The correction of the color term was not applied. Note that the derived magnitudes could be affected by an amount depending on the unknown spectral character of the meteors.} The light curve of each meteor was derived. \revision{0206}{About 20\% of the meteors showed light bursts with amplitudes of larger than $1.5,\mathrm{mag}$ in their light curves, possibly attributed to fragmentation. Such sudden bursts are not necessarily accompanied by variations in the RCS \citep[e.g.,][]{brown_simultaneous_2017}. The meteors obtained in \runB were rarely affected by such bursts since they were observed in a narrow fields-of-view. To enable a fair comparison with the magnitude in \runB, the magnitude averaged over the streak was adopted as the representative value of each meteor, instead of the peak magnitude, since the latter could be affected by fragmentation. Since the bursts were sufficiently short, the variability in the brightness had little effect on the average brightness values.} Finally, the observed magnitudes were converted into the meteor absolute magnitudes.


\subsection{Simultaneous Meteors of \runB}
To extract simultaneous meteors, we first sifted the optical meteors detected within ${\pm}1$ frames (1.5\,s) of the time stamp of each meteor detected by radar. Then, the meteors detected by radar were projected onto the sky from Kiso Observatory (Figure~\ref{fig:results:trajectory}). The candidates of the simultaneous meteors were selected based on the differences in direction and the separations; The angle between the radar (orange) and optical (red) trajectories should be smaller than 2.5\degree and the separation angle of the two trajectories (the blue segment in Figure~\ref{fig:results:trajectory}) should be smaller than 0.25\degree. When the same meteor was detected in multiple detectors, the brightest segment was adopted as the representative one. The total number of unique simultaneous meteors was 485 in the four nights. As shown in Figure~\ref{fig:results:trajectory}, the pointing of the telescope was displaced. The radar and optical observations sometimes traced completely different portions of the trajectory. The magnitudes of such meteors could be erroneous. Thus, we removed about 250 simultaneous meteors whose optical trajectories were not completely covered by the radar observations. \revision{0206}{A possible contribution from meteor showers were removed based on the D-criterion. In total, 5 meteors were removed.} Finally, the sample size was reduced to 228, \revision{0206}{which accounted for about 8\% of the total number of the meteors whose radar trajectories crossed the fields-of-view of Tomo-e Gozen}. In the following sections, the 228 simultaneous sporadic meteors were investigated.

The meteors in \runB were observed at 2\,Hz. Thus, the meteors were captured as streaks. \revision{0206}{We calculated the magnitudes of the meteors, following the method used in \citet{iye_suprimecam_2007}, to derive the brightness of the meteors from the detected streaks. The brightness of the meteor was estimated by $I_v\,v\,T$, where $I_v$ is the line intensity averaged along the streak, $v$ is the angular velocity of the meteor, and $T$ is the exposure time of each video frame.} In \citet{iye_suprimecam_2007}, the meteor speeds $v$ were uniformly assumed to be $10\degree\,\mathrm{s^{-1}}$. The magnitudes of meteors were also derived based on the same assumption in \citet{ohsawa_luminosity_2019}. This assumption was a major source of the uncertainty in those studies. In the \runB observation, the projected motion of each meteor was directly derived from the MU radar observation. The magnitudes in the present research were little affected by the uncertainty in the meteor speed. The magnitudes of the meteors were first calibrated against the $V$-band magnitudes of the UCAC4 catalog \citep{zacharias_fourth_2013}. \revision{0206}{The color term correction was not applied. Note that the derived magnitudes could be affected by an amount dependeng on the unknown spectral character of the meteors.} Then, the magnitudes were finally converted into the meteor absolute magnitudes using the distance between the telescope and the meteors.


\subsection{Statistics of Simultaneous Meteors}

\begin{figure*}
  \centering
  \includegraphics[width=0.9\linewidth]{figs/magnitude_distribution.pdf}
  \caption[Magnitude distributions of the simultaneous meteors]{Magnitude distributions of the meteors simultaneously detected by radar and optically. The top panel shows the magnitude distribution of the observations in 2009--2010 (\runA), while the bottom panel illustrates that of the observations in 2018 (\runB).}
  \label{fig:results:histogram}
\end{figure*}

The distributions of the absolute magnitude is shown in Figure~\ref{fig:results:histogram}. The top panel shows the distribution of \runA, while the bottom panel does that of \runB. \revision{0206}{The brightest meteor in \runA was $1.4\,\mathrm{mag}$, while the faintest meteor was $10.0\,\mathrm{mag}$. The magnitudes at 25-, 50-, and 75-percentiles were $5.0$, $6.1$, and $7.2\,\mathrm{mag}$, respectively. The brightest meteor in \runB was about $2.8\,\mathrm{mag}$, while the faintest meteor was $11.1\,\mathrm{mag}$. The magnitudes at 25-, 50-, and 75-percentiles were $7.4$, $8.1$, and $8.6\,\mathrm{mag}$, respectively.} The detected meteors in \runB were typically about $1.8\,\mathrm{mag}$ fainter than those in \runA, in spite of the larger distance between the radar and optical observation sites. This is simply attributed to the high sensitivity of Tomo-e Gozen.


\begin{figure*}
  \centering
  \includegraphics[width=0.9\linewidth]{figs/meteor_statistics.pdf}
  \caption[Height and Velocity of the simultaneous meteors]{The altitude and the geocentric velocity are shown against the optical magnitude in the $V$-band. The data of \runA are shown by the black circle symbols, while those of \runB are shown by the red square symbols. The top panel illustrates the observed heights in km. The empty and filled symbols are respectively the highest and lowest altitudes measured by the MU radar. The geocentric velocity in $\mathrm{km\,s^{-1}}$ is shown in the bottom panel.}
  \label{fig:results:stats}
\end{figure*}

The top panel of Figure~\ref{fig:results:stats} displays the altitudes of the meteors against the optical absolute magnitudes. \revision{0206}{No apparent dependency of the altitude on the optical magnitude was recognized. But this does not mean the distribution of the meteoroid mass was uniform along the altitude. It should be noted that a limiting meteoroid mass is smaller at a higher altitude for a magnitude-limited sample \citep[e.g., \textit{see} Figure 4 in][]{ceplecha_meteor_1998}.} The altitudes of the \runB meteors are generally lower than those of the \runA meteors. As shown in Figure~\ref{fig:results:trajectory}, the pointing of the telescope in \runB was displaced downward in elevation. The fields-of-view of Tomo-e Gozen were set below about 30\degree in elevation, corresponding to about $100\,\mathrm{km}$ in altitude above the MU radar. Since the differences between the highest and lowest altitudes is typically about $10\,\mathrm{km}$ in Figure~\ref{fig:results:stats}, the meteors below $110\,\mathrm{km}$ were selectively detected as simultaneous meteors in \runB. Thus, the difference in the altitude distributions is explained by the observation bias. Consequently, the present samples contained few meteors which were fainter than about $7\,\mathrm{mag}$ and whose altitudes were higher than $100\,\mathrm{km}$. The geocentric velocities are plotted against the optical magnitudes in the bottom panel of Figure~\ref{fig:results:stats}. No apparent trend is confirmed. The distribution of the geocentric velocity is bimodal.

Figure~\ref{fig:results:radiant} shows the distribution of the meteor radiants in the Hammer projection of the ecliptic latitude and Sun-centered ecliptic longitude coordinates. The meteors whose geocentric velocity is faster than $45\,\mathrm{km\,s^{-1}}$ are shown by the orange symbols. The distribution of the faster population is consistent with the apex sources \citep[e.g,][]{hawkins_radio_1956}, while the slower populations are possibly attributed to the antihelion and north toroidal sources \citep[e.g.,][]{hawkins_radio_1956,stohl_seasonal_1968}. No significant concentration is found in Figure~\ref{fig:results:radiant}, suggesting that possible contributions from meteor showers were successfully removed.


\begin{figure*}
  \centering
  \includegraphics[width=0.9\linewidth]{figs/radiant_distribution.pdf}
  \caption[The radiant distribution of the simultaneous meteors.]{The radiant distribution of the simultaneous meteors in the Sun-centered Ecliptic coordinates. The data of \runA are shown by the circle symbols, while those of \runB are shown by the square symbols. The meteors whose geocentric velocities are faster than $45\,\mathrm{km\,s^{-1}}$ are shown in orange, while the meteors slower than $45\,\mathrm{km\,s^{-1}}$ are shown in green.}
  \label{fig:results:radiant}
\end{figure*}


\begin{figure*}
  \centering
  \includegraphics[width=0.9\linewidth]{figs/rcs_against_magnitude.pdf}
  \caption[The relationship between the RCS and $V$ magnitude.]{The relationship between the radar cross section and the optical absolute magnitude in the $V$-band. The meteors of \runA and \runB are shown by the black circle and red square symbols, respectively. The blue dashed line indicates the linear regression line.}
  \label{fig:results:rcsmag}
\end{figure*}


Figure~\ref{fig:results:rcsmag} shows the optical absolute magnitude against the RCS, \revision{0206}{which is the value at the maximum signal-to-noise ratio (SNR) along the whole radar trajectory. On the other hand, the brightest magnitudes were independently measured in the optical observations. Thus, the section where the RCS was measured and the section where the optical brightness was measured could be different. A possible uncertainty due to this time difference is discussed later.} The data of \runA and \runB seem to follow the same trend, where the optical magnitude became brighter as the RCS bacame larger. The trend was approximated by a linear regression line. Since there was large scatter in Figure \ref{fig:results:rcsmag}, trial regression lines were calculated both in terms of the RCS and the magnitude via a least-square method and then the line with the averaged slope was adopted as the representative regression line:
\begin{equation}
  \label{eq:results:rcsmag}
  M_V = -(0.169{\pm}0.006){\times}A + (4.43{\pm}0.13),
\end{equation}
where $M_V$ is the meteor absolute magnitude in the $V$-band and $A$ is the radar cross section in units of dBsm (decibel relative to $1\,\mathrm{m^2}$). The representative regression line is shown by the blue dashed line in Figure~\ref{fig:results:rcsmag}. \revision{0206}{The uncertainties were estimated by the bootstrapping method. It should be noted that, since the color-term correction was not applied, the present result could suffer from a systematic bias due to different spectral responses of the cameras. Such a possible bias was not taken into account in the uncertainties in Equation~(\ref{eq:results:rcsmag}).}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
