\newcommand{\runA}{ICCD09\xspace}
\newcommand{\runB}{KISO18\xspace}

\subsection{Observations with the MU radar and Intensified CCD camera}

\begin{table*}
  \centering
  \caption[Observation Summary in 2009--2010]{Summary of Observations with the MU radar and ICCD camera in 2009--2010}
  \label{tab:obs:summary:iccd}
  \small
  \begin{tabular}{ll}
    \toprule
    Date & Sep. 24--26, Oct. 19--21, Nov. 8, and Dec. 13--14 in 2009 \\
         & Mar. 11, Aug. 12, Sep. 13, and Dec. 14 in 2010 \\
    Radar System & Middle and Upper Atmosphere Radar (46.5 MHz) \\
    Optical System & Canon 200\,mm F/1.8 and Hamamatsu CCD camera (ORCA-05G) \\
    Video Frame Rate & 29.97\,Hz \\
    Field of View & ${\sim}6\degree$ in diameter (radar),
                    $9.9\degree{\times}10.9\degree$ (optical) \\
    \bottomrule
    \multicolumn{2}{p{.8\linewidth}}{}
  \end{tabular}

  \centering
  \caption[Observation Summary in 2018]{Summary of Observations with the MU radar and Tomo-e Gozen in 2018}
  \label{tab:obs:summary:tomoe}
  \small
  \begin{tabular}{ll}
    \toprule
    Date & Apr. 18--22 in 2018 (11:00--20:00 UT, 36 hours in total) \\
    Radar System & Middle and Upper Atmosphere Radar (46.5 MHz) \\
    Optical System & 1.05-m Kiso Schmidt telescope and Tomo-e Gozen (Q1) \\
    Video Frame Rate & 2.0\,Hz \\
    Field of View & ${\sim}6\degree$ in diameter (radar),
                    $20{\times}~ 39.7'{\times}22.4'$ (optical) \\
    \bottomrule
    \multicolumn{2}{p{.8\linewidth}}{}
  \end{tabular}
\end{table*}

The first observation run was carried out in 2009 and 2010 (hereafter, referred as to \runA). Specifications of the observations are summarized in Table~\ref{tab:obs:summary:iccd}. We had several observations in 2009 and 2010. Radar observations were carried out with Middle and Upper Atmosphere Radar (hereafter, MU radar) in the Shigaraki MU Observatory\footnote{Shigaraki MU Observatory is located at \dms{+34}{51}{14}{5}\,N and \dms{+136}{06}{20}{24}\,E (WGS84).} of Research Institute for Sustainable Humanosphere (RISH), Kyoto University. The MU radar was operated in the general head echo mode \citep{kero_first_2011}. Optical observations were carried out with an image-intensified CCD camera made by Hamamatsu equipped with a Canon 200\,mm F/1.8 lens. The optical camera system was set up in Shigaraki. The camera was pointed toward zenith and continuously monitored the sky at 29.97\,Hz. GPS time stamps were imprinted in the video data.

The radar data were reduced using a standard data reduction process of the MU radar \citep{kero_first_2011,kero_2009-2010_2012,kero_meteor_2012}. The three dimensional trajectory and the radar cross section (RCS) \revision{0206}{along the trajectory} of each meteor were obtained. Meteors in the optical data were detected with a time shifted motion capture software, UFOCapture\footnote{UFOCaptrueV2 (ver 2.24) in \url{http://sonotaco.com/soft/e_index.html}}. The optical trajectories and the magnitudes of the meteors were derived with a post processing tool, UFOAnalyzer\footnote{UFOAnalyzerV2 (ver 2.44) in \url{http://sonotaco.com/soft/e_index.html}}.


\subsection{Observations with the MU radar and Tomo-e Gozen}

The second observations were carried out in April, 2018 (hereafter, referred as to \runB). Radar observations were also carried out with the MU radar in the same setting as in \runA. Optical observations were carried out with a mosaic CMOS camera, Tomo-e Gozen, mounted on the 1.05-m Schmidt Telescope in Kiso Observatory\footnote{Kiso Observatory is located at \dms{+35}{47}{38}{7}\,N and \dms{+137}{37}{42}{2}\,E (WGS84).} of the Institute of Astronomy, the University of Tokyo. Specifications of the observations are summarized in Table~\ref{tab:obs:summary:tomoe}. Tomo-e Gozen is equipped with 84 CMOS image sensors of $2000{\times}1128$ pixels in size. The field-of-view is, in total, as large as about $20\,\mathrm{sq.\,degree}$, and Tomo-e Gozen is able to monitor the sky up to at 2\,Hz \citep{sako_development_2016,sako_tomo-e_2018,kojima_evaluation_2018}. The readout of the image sensor is synchronized with the GPS time and the time stamp of Tomo-e Gozen is as accurate as 0.2\,ms \citep{sako_tomo-e_2018}. Observations are carried out in a clear filter and a nominal limiting magnitude for stars is about 18\,mag, which is corresponding to about 12\,mag for meteors \citep{ohsawa_luminosity_2019}. At the time of the observations, only one quadrant of the camera was available and one sensor was not operating. Thus, the observations were carried out with 20 CMOS sensors (${\sim}4.8\,\mathrm{sq.\,degree}$ in total). The Kiso Schmidt telescope was pointed toward the sky 100\,km above the MU radar. Since the telescope tracked the sky, the direction of the telescope was adjusted every 3 minutes. Thus, the length of each video is 3 minutes. Kiso Observatory is located about 173\,km distant from the Shigaraki MU Observatory. The elevation angle of the telescope was about 30\degree and the distance between the telescope and the volume monitored by the MU radar was about 200\,km.

\begin{figure*}
  \centering
  \includegraphics[width=\linewidth]{figs/meteor_trajectories_all.pdf}
  \caption[Meteors at Kiso Observatory]{The meteors detected in \runB are projected onto the sky from Kiso Observatory in the elevation and azimuth coordinates. Each panel illustrates the meteors detected in a night. The orange segments indicate the meteors detected by the MU radar; The filled and empty circles are, respectively, the first and last detection points. The gray dashed lines are the extensions of the MU trajectories for reference. The gray rectangles are the fields-of-view of Tomo-e Gozen. The red segments indicate the meteors detected by Tomo-e Gozen. The blue segments indicate the distances between the meteor segments detected by the MU radar and Tomo-e Gozen. The violet, green, and navy circles respectively indicate the center of the field-of-view of the MU radar at $100$, $110$, and $120\,\mathrm{km}$ in altitude.}
  \label{fig:results:trajectory}
\end{figure*}

The radar data were reduced in the same manner as in \runA. \revision{0206}{The three dimensional trajectory and the RCS of each meteor were obtained.} The optical data were reduced in a standard astronomical data reduction procedure for imaging observations: a dark frame was subtracted and a flat frame correction was applied. Meteors in the optical video data were extracted with an algorithm based on the Hough transformation \citep{ohsawa_development_2016,ohsawa_luminosity_2019}. The detected events are summarized in Figure~\ref{fig:results:trajectory}. The meteors detected by the MU radar are shown by the orange segments, while those detected by Tomo-e Gozen are shown by the red segments. The gray rectangles indicate the fields-of-view of Tomo-e Gozen. A number of rectangles appear in Figure~\ref{fig:results:trajectory} since the telescope was moved frequently. The violet, green, and navy circles in Figure~\ref{fig:results:trajectory} indicate the center of the field-of-view of the MU radar at different altitudes for reference. This indicates that the fields-of-view of Tomo-e Gozen were, however, slightly displaced due to a miscalculation.


\subsection{Archival Observations from the MU radar Meteor Head Echo database}
Meteors detected by the MU radar were retrieved from an archival data, in order to discuss the luminosity function of faint meteors. Part of data were already published in \citep{kero_2009-2010_2012,kero_first_2011} and available in the MU Radar Head Echo Database (MURMHED). All the data were reduced along with \citet{kero_meteor_2012}. The observations were carried out during 2009--2015 and the total observing time was 845.8\,hour. The archive contains 157043 meteor events in total. \revision{0206}{The contributions from meteor showers were removed based on the D-criterion\footnote{\revision{0206}{The calculation was based on the orbital elements of the 112 established meteor showers issued in the IAU Meteor Data Center on February 17, 2020.}}, which is a criterion to determine whether a meteor belongs to a stream or not \citep{southworth_statistics_1963}. The threshold of the D-criterion was set to 0.2. This threshold is relatively weak compared to previous studies which identify meteor showers by the D-criterion \citep[e.g.,][]{jenniskens_cams_2016}, but reasonable to roughly estimate the contributions from meteor showers \citep{andreic_results_2014,segon_results_2014,gural_results_2014,andreic_ten_2013}. Some sporadic meteors may be wrongly removed due to the weak threshold, but the result will not be affected. The removed meteors were mainly composed of Geminids (1034), Orionids (2049), and $\eta$ Aquariids (1900). Finally, 150080 meteors were extracted as sporadic meteors.} The archive was complied with several campaign observations. The number of events per month is listed in Table~\ref{tab:observation:murmhed}. The table indicate that the observations were biased toward meteors observed in October and December.

\begin{table*}
  \centering
  \caption{The Number of Sporadic Meteor Events in MURMHED}
  \label{tab:observation:murmhed}
  \begin{tabular}{cllllllllllll}
    \toprule
    \multicolumn{1}{@{\quad}c@{\quad}}{Year}
    & \multicolumn{1}{c}{Jan.}
    & \multicolumn{1}{c}{Feb.}
    & \multicolumn{1}{c}{Mar.}
    & \multicolumn{1}{c}{Apr.}
    & \multicolumn{1}{c}{May}
    & \multicolumn{1}{c}{Jun.}
    & \multicolumn{1}{c}{Jul.}
    & \multicolumn{1}{c}{Aug.}
    & \multicolumn{1}{c}{Sep.}
    & \multicolumn{1}{c}{Oct.}
    & \multicolumn{1}{c}{Nov.}
    & \multicolumn{1}{c}{Dec.} \\\midrule
    2009
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{3819}
    & \multicolumn{1}{c}{5465}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{5419}
    & \multicolumn{1}{c}{9071}
    & \multicolumn{1}{c}{4925}
    & \multicolumn{1}{c}{4632} \\
    2010
    & \multicolumn{1}{c}{3398}
    & \multicolumn{1}{c}{2788}
    & \multicolumn{1}{c}{2000}
    & \multicolumn{1}{c}{2205}
    & \multicolumn{1}{c}{2479}
    & \multicolumn{1}{c}{3064}
    & \multicolumn{1}{c}{3570}
    & \multicolumn{1}{c}{8520}
    & \multicolumn{1}{c}{4441}
    & \multicolumn{1}{c}{21467}
    & \multicolumn{1}{c}{4856}
    & \multicolumn{1}{c}{8314} \\
    2011
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{6087}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---} \\
    2012
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{10930}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---} \\
    2013
    & \multicolumn{1}{c}{3298}
    & \multicolumn{1}{c}{2231}
    & \multicolumn{1}{c}{1708}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{13332} \\
    2014
    & \multicolumn{1}{c}{3300}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{136}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{6477} \\
    2015
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{2148}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---}
    & \multicolumn{1}{c}{---} \\
    \midrule
    total
    & \multicolumn{1}{c}{9996}
    & \multicolumn{1}{c}{5019}
    & \multicolumn{1}{c}{3708}
    & \multicolumn{1}{c}{4353}
    & \multicolumn{1}{c}{2615}
    & \multicolumn{1}{c}{6883}
    & \multicolumn{1}{c}{9035}
    & \multicolumn{1}{c}{8520}
    & \multicolumn{1}{c}{9860}
    & \multicolumn{1}{c}{47555}
    & \multicolumn{1}{c}{9781}
    & \multicolumn{1}{c}{32755} \\
    \bottomrule
  \end{tabular}
\end{table*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
