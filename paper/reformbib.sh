#!/bin/bash

SRC=${1:?}

cat ${SRC} \
    | sed -e '/^\\begin/d' \
    | sed -e '/^\\expand/d' \
    | sed -e '/^\\end/d' \
    | sed -e '/\\def/d' \
    | tr -d '\n' \
    | sed -e 's/\\bibitem/\n/g' \
    | sed -e 's/^\[.*\]//' \
    | sed -e 's/^{[a-z_0-9\-]*}//' \
    | sed -e 's/\\newline//g' \
    | sed -e 's/\\natexlab//g' \
    | sed -e 's/\\urlprefix//g' \
    | sed -e 's/\\url/ URL /g' \
    | sed -e 's/\\./ /g' \
    | sed -e 's/~/ /g' \
    | sed -e 's/{\|}//g' \
    | sed -e '/^\s*$/d' \
    | awk '{print NR".", $0}'
