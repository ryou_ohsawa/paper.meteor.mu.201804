The manuscript 'Relationship between Radar Cross Section and Optical Magnitude based on Radar and Optical Simultaneous Observations of Faint Meteors' is discussed.

First, I wish to state that to properly make a study like this is not an easy task.  As well, it can be very difficult to properly review (or even read) a paper on simultaneous observations, as it requires expertise in multiple observational techniques.  While the topic is very important to meteor science, it seems to have lost popularity in modern times.  So I thank the authors for working on such a difficult but important topic.

However, while the authors do give a summary of what they have done, their manuscript is full of unjustified assumptions, and does not deliver on the key results which they could.

#### Specific comments :

- you present Figure 8, which shows the cumulative influx (which should be written "number flux") as a function of mass.  Yet, you do not integrate this curve to give an appropriate mass influx for comparison to the 5-300 metric tons per day which you mention at the start of the paper.  Arguably mass is the most important physical property.

- similarly, you relate a radar RCS to an optical magnitude, yet what use is the radar RCS to the physics being studied ?  How does the RCS relate to mass, and what scattering model assumptions must be made ?  Some level of detail beyond a reference to the work on Kero must be given.

- you use V band magnitudes, yet it is likely your cameras are more sensitive to R magnitudes.  Was any attempt made to quantify the color term error ?  The meteors are stars are not spectrally similar.

- you have Equation (3) which is from 1956 and is based on many assumptions, some of which may not be relevant to the instruments of today.  There is no discussion on why this equation would be appropriate to use, nor on any 'luminous efficiency'

- you remove meteor showers using a D-criterion comparison (but the Lyrids remained) - why do you not include meteor showers in your analysis?  Might there be others you have not removed ? They represent a common target sharing the same phyical and chemical properties.

- Figure 3 on page 11 shows the meteor altitude as a function of V-band magnitude.  The text states that no apparent trend is confirmed, but yet the meteors will all be at different mass limits.  This needs to be discussed.

- you state "The magnitude averaged over the streak was adopted as the representative magnitude of each meteor" - by how much did the magnitude of a single meteor vary?  What might you expect the error to be ?

- "Faint meteors" (in the abstract) should be defined with an appropriate magnitude or mass range

- ".. are ionized and excited by an interplanetary dust grain ..." - should the amount of such activity be quantified ?  There is really no mention of conversion efficiency.

-  "... the importance of simultaneous optical and radar observations cannot be overemphasized."  - this does not state anything.

- you give a brief summary of past works, except for Weryk and Brown (2013) which seem to have made the largest contribution.  What overall unanswered questions are there regarding simultaenous observations ?

- how often is a radar meteor detected in the common volume for which there is no optical meteor ?

- "The threshold for the D-criterion was set at 0.2".  Is that a large value ?  How does it compare to what other authors might choose ?

- "We adopted the vidoe rate magnitudes defined in Iye et al. (2007)"  - it is not clear what this text is referring to or why this is needed.

- "The brightest meteor in ICCD09 was 1.38 mag" - that seems overly precise given the likely uncertainties.

- the uncertainties in Equation (1) do not seem realistic given the scatter in Figure 5.

- "This is in part because the RCS was not measured exactly at the same time as the optical magnitude".  What exactly then are you comparing and how does it affect your results ?

- ".. has been widely approximated by an exponential function" - this equation results from assuming the population is distributed as a power law.
