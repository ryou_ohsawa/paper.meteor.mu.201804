## response to the reviewer's comments

We appreciate the reviewer's careful reading and constructive comments. The manuscript has been revised to basically follow the reviewer's comments. The revised parts of the manuscript is highlighted in red.

We have found that the calculation of the D-criterion was partly wrong. In the current version, the contribution from meteor showers have been successfully removed based on the latest orbital elements. The number of the meteors have been updated. The figures have been updated accordingly, but the overall results and discussion are not affected.


The responses to the specific comments are as follows:


> It is worth to add some other references of radio optical works. The reviewer is aware that not all of them are of the same quality, but it is always challenging to conduct this type of joined observations. Please consider adding further references on radar-optical experiments from other instruments (PFISR (Alaska), SAAMER (RioGrande)) to get a more international balance. However, the reviewer will not provide a recommendation on what to cite.
We have added two more optical and radar studies: one using SAAMER (Michell et al., 2015) and one using the Arecibo radar (Michell et al., 2019). We have revised the manuscript to describe the work with PFISR (Michell, 2010) more in detail.


> Please provide more information in short on how the RCS is obtained and defined. Do you only compare the RCS vs optical magnitude from the commonly detected part of the trajectory? Or do you use the peak RCS from the radar data vs. peak magnitude from the optical, although they might belong to different parts along the meteoroid flight path? I suggest providing a brief outline of what is done. Due to the scattering in the log-log plot I don’t expect much change, but it is very obvious from Brown et al., 2017 that the RCS as well as the optical brightness seems to be variable along the flight track. In particular, fragmentation causes sudden light bursts, but might not lead to a corresponding change in the RCS or vice versa. Please also add these effects to the discussion.
We have explicitly described that the RCS was measured at the peak SNR along the whole radar trajectory and compared with the peak optical magnitude. We have added the discussion on the variations in the RCS and optical magnitude and the discussion on the fragmentation. We have suggested that the large scatter in Figure 5 is partly attributed to the fragmentation.


> The authors already discuss many potential limitations of the presented extrapolation from the RCS vs. opt. mag. However, they claim to see a diurnal variation in the population indices. I suggest adding another important aspect of their analysis. The optical observations are only available during night-time. At the latitude of Shigaraki the apex sources are the dominating throughout the year. They provide the fast meteoroid component of their bimodal velocity distribution, but the apex source is not present during the afternoon hours. However, the detectability of a meteoroid strongly depends on their kinetic energy, hence, during the afternoon you observe a more-heavy(larger) part of the meteor population than during the night. As the population index is supposed to be mass dependent (Grun model) and your cross-calibration is for sure mass dependent, this might introduce the diurnal pattern. The reviewer suggests mentioning this aspect as well in your discussion of the limitations.
We have added a discussion on a possible origin of the diurnal variation due to the observational bias following the comment.


> you present Figure 8, which shows the cumulative influx (which should be written "number flux") as a function of mass.  Yet, you do not integrate this curve to give an appropriate mass influx for comparison to the 5-300 metric tons per day which you mention at the start of the paper.  Arguably mass is the most important physical property.
We have added an estimated mass flux to the Earth surface.


> similarly, you relate a radar RCS to an optical magnitude, yet what use is the radar RCS to the physics being studied?  How does the RCS relate to mass, and what scattering model assumptions must be made?  Some level of detail beyond a reference to the work on Kero must be given.
We have added a paragraph to discuss the relationship between the radar-cross-section from a head echo and the optical brightness.


> you use V band magnitudes, yet it is likely your cameras are more sensitive to R magnitudes.  Was any attempt made to quantify the color term error?  The meteors are stars are not spectrally similar.
We have added a sentence to make clear that the correction of the color term was not applied, and that the derived magnitudes could be affected by an amount depending on the unknown spectral character of the meteors. Although the magnitude can be affected by the color of the meteors, the overall results and discussion are not to be affected.


> you have Equation (3) which is from 1956 and is based on many assumptions, some of which may not be relevant to the instruments of today.  There is no discussion on why this equation would be appropriate to use, nor on any 'luminous efficiency'
The calculation of the meteoroid mass has been renewed. The brightness of a meteoroid is calculated from the thermal ablation theory, following the procedure given by Hill et al. (2005). The brightest magnitudes are calculated for different meteoroid parameters, and then, a relationship to estimate the meteoroid mass from the magnitude, initial velocity, and radiant zenith angle is derived. The meteoroid mass is derived from the derived relationship. The discussion on the meteoroid mass has been updated accordingly. While the mass range has been changed, the overall discussion is not affected.


> you remove meteor showers using a D-criterion comparison (but the Lyrids remained) - why do you not include meteor showers in your analysis?  Might there be others you have not removed? They represent a common target sharing the same physical and chemical properties.
Sporadic meteors and meteor showers have different luminosity or mass functions. It seems reasonable to remove contributions from the meteor showers to keep the discussion simple. The calculation of the D-criterion has been revised. The meteor showers are appropriately removed. We have added sentences to describe which orbital elements were used. In the first observation run, no Lyrids were somehow detected.


> Figure 3 on page 11 shows the meteor altitude as a function of V-band magnitude.  The text states that no apparent trend is confirmed, but yet the meteors will all be at different mass limits.  This needs to be discussed.
We have added sentences to note that the meteoroid mass does not distribute uniformly along with altitudes.


> you state "The magnitude averaged over the streak was adopted as the representative magnitude of each meteor" - by how much did the magnitude of a single meteor vary?  What might you expect the error to be?
About 20% of the meteors showed bursts with amplitudes larger than 1.5 mag. The error due to the variability was small since the bursts were sufficiently short. We have updated the manuscript accordingly.


> "Faint meteors" (in the abstract) should be defined with an appropriate magnitude or mass range
We have described the magnitude range explicitly.


> ".. are ionized and excited by an interplanetary dust grain ..." - should the amount of such activity be quantified?  There is really no mention of conversion efficiency.
The manuscript has been revised accordingly. The meteoroid mass is calculated based on the ablation theory.


>  "... the importance of simultaneous optical and radar observations cannot be overemphasized."  - this does not state anything.
Removed as suggested.


> you give a brief summary of past works, except for Weryk and Brown (2013) which seem to have made the largest contribution.  What overall unanswered questions are there regarding simultaenous observations?
The manuscript has been updated to more appropriately refer to the contributions of Weryk and Brown (2012, 2013) in simultaneous observations. The main purpose of the current study has been written in the following paragraph.


> how often is a radar meteor detected in the common volume for which there is no optical meteor?
The optical counterparts were detected for about 8\% of the meteors detected in radar and crossed the fields-of-view in optical. We've explicitly described the detection rate in optical.


> "The threshold for the D-criterion was set at 0.2".  Is that a large value?  How does it compare to what other authors might choose?
This weak threshold seems reasonable to roughly remove the contributions from the meteor showers. Some references have been added. Some meteors could be wrongly removed due to the weak threshold, but the number of the removed meteors was small. We have added a sentence to clearly explain that the result and discussion are not affected by the weak threshold.


> "We adopted the vidoe rate magnitudes defined in Iye et al. (2007)"  - it is not clear what this text is referring to or why this is needed.
We have added sentences to describe the way that the brightness of the meteors were derived from the trailed streaks in the video data.


> "The brightest meteor in ICCD09 was 1.38 mag" - that seems overly precise given the likely uncertainties.
Corrected as suggested.


> the uncertainties in Equation (1) do not seem realistic given the scatter in Figure 5.
The calculation of the uncertainties has been updated based on the bootstrapping method, to properly take into account the scatter. The updated uncertainties seem reasonable. But, as pointed out by the referee, the large scatter remains to be explained.  We have added sentences to discuss the possibility that the RCS-magnitude relationship is variable. In case that the relationship is variable, the present relationship is regarded as the averaged one.


> "This is in part because the RCS was not measured exactly at the same time as the optical magnitude".  What exactly then are you comparing and how does it affect your results?
We have described how the RCS in Figure 5 were measured and that the RCS at the maximum SNR and the peak magnitude were compared. The section where the RCS was measured and the section where the magnitude was measured were not exactly the same. Sudden variations in the RCS and the magnitude due to the fragmentation possibly contributed to the large scatter in Figure 5. The overall results and discussion are not affected.


> ".. has been widely approximated by an exponential function" - this equation results from assuming the population is distributed as a power law.
Corrected as suggested.
