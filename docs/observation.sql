.separator ,

DROP TABLE IF EXISTS muradar_detections;
CREATE TABLE muradar_detections (
  event_id INTEGER NOT NULL,    --- Event ID
  year_jst INTEGER NOT NULL,    --- Year
  month_jst INTEGER NOT NULL,   --- MonthH
  day_jst INTEGER NOT NULL,     --- Day
  hour_jst INTEGER NOT NULL,    --- Hour
  minute_jst INTEGER NOT NULL,  --- Minute
  second_jst INTEGER NOT NULL,  --- Second
  duration FLOAT NOT NULL,      --- Duration (s)
  altitude_h FLOAT NOT NULL,    --- Initial Altitude (km)
  altitude_l FLOAT NOT NULL,    --- Last Altitude (km)
  azimuth FLOAT NOT NULL,       --- Azimuth angle (deg)
  zangle FLOAT NOT NULL,        --- Zenith Distance (deg)
  velocity FLOAT NOT NULL,      --- Initial velocity (km/s)
  data_if INTEGER NOT NULL,     --- #data for interferrometry
  data_dp INTEGER NOT NULL      --- #data for doppler ranging
);
.import 'MUradar_detections_processed_on_20180509.txt' muradar_detections

DROP TABLE IF EXISTS muradar_timestamp;
CREATE TABLE muradar_timestamp (
  event_id INTEGER NOT NULL,    --- Event ID
  timestamp TEXT NOT NULL       --- UTC timestamp (YYYY-mm-ddTHH:MM:SS.ssss)
);
.import 'MUradar_detections_timestamp_utc.txt' muradar_timestamp

.separator |


DROP VIEW IF EXISTS muradar_detections_utc;
CREATE VIEW muradar_detections_utc AS
SELECT
  u.event_id,   --- Event ID
  u.timestamp,  --- UTC timestamp (YYYY-mm-ddTHH:MM:SS.ssss)
  d.duration,   --- Duration (s)
  d.altitude_h, --- Initial Altitude (km)
  d.altitude_l, --- Last Altitude (km)
  d.azimuth,    --- Azimuth angle (deg)
  d.zangle,     --- Zenith Distance (deg)
  d.velocity,   --- Initial velocity (km/s)
  d.data_if,    --- #data for interferrometry
  d.data_dp     --- #data for doppler ranging
FROM muradar_timestamp AS u, muradar_detections as d
WHERE u.event_id = d.event_id;


DROP TABLE IF EXISTS tomoe_timestamp;
CREATE TABLE tomoe_timestamp (
  filename TEXT NOT NULL,       --- Tomo-e Gozen filename
  detector_id INTEGER NOT NULL, --- Detector ID
  timestamp TEXT NOT NULL       --- UTC timestamp (YYYY-mm-ddTHH:MM:SS.ssssss)
);
.import '../observation/20180418/docs/timestamp_20180418.tbl' tomoe_timestamp
.import '../observation/20180419/docs/timestamp_20180419.tbl' tomoe_timestamp
.import '../observation/20180420/docs/timestamp_20180420.tbl' tomoe_timestamp
.import '../observation/20180421/docs/timestamp_20180421.tbl' tomoe_timestamp

.separator ' '
DROP TABLE IF EXISTS simultaneous_event_candidates;
CREATE TABLE simultaneous_event_candidates (
  mu_timestamp TEXT NOT NULL,      --- UTC timestamp (MU radar)
  detection_filename TEXT NOT NULL --- Tomo-e Gozen filename
);
.import '../observation/20180418/docs/simultaneous_detection_candidates_20180418.lst' simultaneous_event_candidates
.import '../observation/20180419/docs/simultaneous_detection_candidates_20180419.lst' simultaneous_event_candidates
.import '../observation/20180420/docs/simultaneous_detection_candidates_20180420.lst' simultaneous_event_candidates
.import '../observation/20180421/docs/simultaneous_detection_candidates_20180421.lst' simultaneous_event_candidates


DROP TABLE IF EXISTS detected_streaks_raw;
CREATE TABLE detected_streaks_raw (
  detection_filename TEXT NOT NULL --- Tomo-e Gozen filename
);
.import '../observation/20180418/docs/detected_streak_20180418.lst' detected_streaks_raw
.import '../observation/20180419/docs/detected_streak_20180419.lst' detected_streaks_raw
.import '../observation/20180420/docs/detected_streak_20180420.lst' detected_streaks_raw
.import '../observation/20180421/docs/detected_streak_20180421.lst' detected_streaks_raw

.separator |

DROP TABLE IF EXISTS detected_streaks_info;
CREATE TABLE detected_streaks_info (
  detection_filename TEXT NOT NULL, --- Tomo-e Gozen filename
  real_streak TEXT NOT NULL,        --- T/F contains meteor-like streaks
  number_streaks INTEGER NOT NULL,  --- number of streaks in the frame
  comment TEXT NOT NULL             --- comment
);
.import '../observation/20180418/docs/detected_streak_info_20180418.lst' detected_streaks_info
.import '../observation/20180419/docs/detected_streak_info_20180419.lst' detected_streaks_info
.import '../observation/20180420/docs/detected_streak_info_20180420.lst' detected_streaks_info
.import '../observation/20180421/docs/detected_streak_info_20180421.lst' detected_streaks_info

DROP VIEW IF EXISTS detected_streaks;
CREATE VIEW detected_streaks AS
SELECT
  r.detection_filename AS filename,
  CAST(substr(r.detection_filename,1,4) AS INTEGER) AS exposure_id,
  CAST(substr(r.detection_filename,5,2) AS INTEGER)+100 AS detector_id,
  CAST(substr(r.detection_filename,8,3) AS INTEGER) AS frame_number,
  i.real_streak as real_streak,
  i.number_streaks as number_streaks,
  i.comment as comment
FROM detected_streaks_raw AS r, detected_streaks_info AS i
WHERE r.detection_filename == i.detection_filename;

DROP VIEW IF EXISTS simultaneous_detections;
CREATE VIEW simultaneous_detections AS
SELECT
  s.mu_timestamp AS timestamp,
  d.exposure_id,
  d.detector_id,
  d.frame_number,
  d.number_streaks,
  d.comment
FROM detected_streaks AS d, simultaneous_event_candidates AS s
WHERE d.filename == s.detection_filename AND d.real_streak == 'T';

DROP VIEW IF EXISTS simultaneous_detections_with_mu;
CREATE VIEW simultaneous_detections_with_mu AS
SELECT
  s.timestamp,
  s.exposure_id,
  s.detector_id,
  s.frame_number,
  s.number_streaks,
  m.event_id,
  m.duration,
  m.altitude_h,
  m.altitude_l,
  m.azimuth,
  m.zangle,
  m.velocity,
  m.data_if,
  m.data_dp
FROM
  simultaneous_detections AS s
LEFT JOIN
  muradar_detections_utc AS m
ON
  substr(s.timestamp,1,21) == substr(m.timestamp,1,21);

