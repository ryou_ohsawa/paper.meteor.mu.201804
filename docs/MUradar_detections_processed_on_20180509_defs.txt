%% Start time of events are given.
%1. [JST] Year
%2. [JST] Month
%3. [JST] Day
%4. [JST] Hour
%5. [JST] Minute
%6. [JST] Second
%7. [s] duration
%8. [km] start altitude
%9. [km] end altitude
%10. [degrees] Radiant azimuth
%11. [degrees] Radiant zenith distance
%12. [km/s] Initial velocity data point
%13. Number of data points used in interferometry
%14. Number of data points used in doppler/range
