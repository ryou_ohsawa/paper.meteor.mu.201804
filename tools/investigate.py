#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, absolute_import, division
from argparse import ArgumentParser as ap
from subprocess import check_call as cc
from datetime import datetime
from copy import deepcopy
import astropy.io.fits as fits
import os.path as path
import sys


def eprint(line):
  print(line, file=sys.stderr)


def Boolean(arg):
  if arg is None: raise ValueError('cannot convert None into boolean')
  if isinstance(arg, bool) or isinstance(arg, int): return bool(arg)
  if isinstance(arg, str) or isinstance(arg, unicode):
    if arg.lower() in ('y', 'yes', 'ok', 't', 'true'): return True
    elif arg.lower() in ('n', 'no', 'ng', 'f', 'false'): return False
  raise ValueError('cannot convert "{}" into boolean'.format(arg))


class QuitProcessing(Exception):
  pass

class MovePrevEntry(Exception):
  pass

class MoveNextEntry(Exception):
  pass



def input_or_quit(prompt):
  ret = raw_input('{} '.format(prompt.strip()))
  if ret in ('quit', 'q', 'exit', 'e'):
    raise QuitProcessing('abort the current process')
  elif ret in ('p', 'prev'):
    raise MovePrevEntry('go back to the prevous entry')
  elif ret in ('n', 'next'):
    raise MoveNextEntry('skip the curent entry and go next')
  return ret


def question_meteor(**options):
  while True:
    r = input_or_quit('input number of meteros:')
    try:
      number = int(r)
      break
    except Exception as e:
      eprint(str(e))
      eprint('invalid answer, input again...')

  return number


def question_comment(**options):
  ret = input_or_quit('input comment:')
  return ret


def log_formatter(contains, num, comment=''):
  timestamp = datetime.utcnow().isoformat()
  return '### {}\n{}\n{}\n{}\n'.format(
    timestamp, 'T' if contains else 'F', number, comment)


def call(commands):
  try:
    cc(commands)
  except Exception as e:
    eprint(str(e))

def xpaset(*args):
  cmd = ['xpaset', '-p', 'ds9']
  cmd.extend(map(str,args))
  call(cmd)


if __name__ == '__main__':
  parser = ap()
  parser.add_argument('catalog')

  parser.add_argument('--noskip', dest='skip', action='store_false')

  args = parser.parse_args()

  with open(args.catalog) as f:
    catalog = [rec.strip().split() for rec in f]

  pos, prev_flag = 0, False
  while True:
    if pos >= len(catalog):
      eprint('complete all the entries!')
      break

    timestamp, filename = catalog[pos]
    subfile = 'sub{}'.format(filename)
    logfile = filename.replace('.fits','.log')
    expid, frame = map(int, filename.replace('.fits','').split('_'))

    if prev_flag is False:
      if path.exists(logfile) and args.skip:
        pos += 1
        continue
    else:
      prev_flag = False

    eprint('display "{}"...'.format(filename))

    if path.exists(filename):
      hdu = fits.open(filename)
      table = hdu[1].data
      snr = table['snr']
      xpos = table['x_position']
      ypos = table['y_position']
      xpaset('cd', path.abspath('.'))
      xpaset('frame', 'frameno', 1)
      xpaset('file', filename)
      for s,x,y in zip(snr,xpos,ypos):
        reg = '{{circle {} {} {}}}'.format(x,y,s/50.0)
        xpaset('regions','command',reg)
      xpaset('zoom', 'to', 'fit')
      xpaset('cmap', 'value', 1, 0.5)
      xpaset('scale', 'limits', 0, 10)
      xpaset('frame', 'frameno', 2)
      xpaset('file', subfile)
      xpaset('wcs', 'align', 'no')
      xpaset('rotate', 'to', 0)
      xpaset('zoom', 'to', 'fit')
      xpaset('cmap', 'value', 1, 0.5)
      xpaset('scale', 'mode', 99.5)
    else:
      eprint('{} not exist'.format(filename))

    try:
      number = question_meteor()
      comment = question_comment()
    except QuitProcessing:
      eprint('abort processing')
      break
    except MovePrevEntry as e:
      eprint(str(e))
      pos -= 1
      prev_flag = True
      continue
    except MoveNextEntry as e:
      eprint(str(e))
      pos += 1
      continue

    with open(logfile,'a') as f:
      f.write(log_formatter(number>0, number, comment))

    pos += 1

