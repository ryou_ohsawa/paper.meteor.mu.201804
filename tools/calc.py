#!/usr/bin/env python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser as ap
from astropy.coordinates import SkyCoord, AltAz, EarthLocation
from astropy.time import Time
import astropy.units as u
import time


def get_location_MURadar():
  u'''
  Obtain an EarthLocation object for RISH MU Radar

  :return: An EarthLocation object for RISH MU Radar
  '''
  longitude = '136d06m32s' #'+136d06m20.105s'
  latitude  = '+34d51m08s' #'+34d51m14.613s'
  elevation = 400 * u.meter
  return EarthLocation.from_geodetic(longitude, latitude, elevation)


def get_location_KisoObs():
  u'''
  Obtain an EarthLocation object for U-Tokyo Kiso Observatory

  :return: An EarthLocation object for U-Tokyo Kiso Observatory
  '''
  longitude = '+137d37m42.200s'
  latitude  = '+35d47m38.700s'
  elevation = 1130 * u.meter
  return EarthLocation.from_geodetic(longitude, latitude, elevation)


if __name__ == '__main__':
  parser = ap(
    description=u'''Calculate Celestial Coordinates for coordinated
    observations with Kyoto University RISH MU radar.
    This script calculates the (ra, dec) coordinates of the region 100-km
    above the RISH MU radar, as well as the (azimuth, altitude) angle pair
    and the distance to the region at that moment.''')
  parser.add_argument('--time', dest='time', type=Time,
                      help=u'Specify the observing time [default=now]')
  args = parser.parse_args()

  kiso = get_location_KisoObs()
  mu = get_location_MURadar()
  mu_100km = EarthLocation(mu.lon,mu.lat,mu.height+100*u.km)

  T = args.time or Time.now()
  schmidt = AltAz(obstime=T, location=kiso)
  pos,vel = mu_100km.get_gcrs_posvel(obstime=T)
  pos0,vel0 = kiso.get_gcrs_posvel(obstime=T)
  target = SkyCoord(pos-pos0, frame='gcrs', obstime=T, obsgeoloc=pos0)
  radec, altaz = target.gcrs, target.transform_to(schmidt)

  ra   = radec.ra.to_string(unit=u.hour,pad=True,precision=2)
  dec  = radec.dec.to_string(unit=u.deg,pad=True,precision=2,alwayssign=True)
  az   = altaz.az.to_string(unit=u.degree,decimal=True,pad=True,precision=2)
  alt  = altaz.alt.to_string(unit=u.deg,decimal=True,pad=True,precision=4)
  dist = altaz.distance.to(u.km)

  print('ra       :  {}'.format(ra))
  print('dec      : {}'.format(dec))
  print('azimuth  :  {}'.format(az))
  print('altitude :  {}'.format(alt))
  print('distance :  {}'.format(dist))
